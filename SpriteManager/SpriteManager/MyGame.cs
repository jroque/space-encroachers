using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace SpaceInvaders
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class JGame : Microsoft.Xna.Framework.Game
    {
        private enum GameState
        {
            MainMenu,
            SpaceInvaders
        }

        public GraphicsDeviceManager graphics;
        private Screen currentScreen;
        private GameState nextGameState;        
        private GOManager SBGManager = GOManager.Instance;        

        #region Singleton Methods
        public static JGame Instance = new JGame();
        private JGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = Constants.SCREEN_WIDTH;
            graphics.PreferredBackBufferHeight = Constants.SCREEN_HEIGHT;
        }
        #endregion

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            InternalResourceLoader.Init(JGame.Instance);
            HUD.Instance.Initialize();
            SoundWave.Initialize();
            base.Initialize();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            currentScreen.Dispose();
            GOManager.Instance.UnloadAll();
            Console.Out.WriteLine("TextureManager: Total Textures - " + TextureManager.getInstance().GetMaxTextures());
            TextureManager.getInstance().UnloadAll();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (Input.IsKeyDown(Keys.Tab, true))
            {
                SoundWave.Play("SMW_Coin");
                GameData.NUM_CREDITS++;
            }

            if (currentScreen == null)
            {
                // Load the default screen
                currentScreen = this.LoadScreen(GameState.MainMenu);
            }
            else
            {
                if (currentScreen.ReadyToExit == true)
                {
                    // Dispose of the current screen
                    currentScreen.Dispose();

                    // Load the next screen
                    currentScreen = this.LoadScreen(nextGameState);

                }
                else
                {
                    // Update the current screen
                    currentScreen.Update(gameTime);
                }
            }

            Input.Update();
            SoundWave.Update();
            HUD.Instance.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);
            currentScreen.Draw(gameTime);
            HUD.Instance.Draw(gameTime);
            base.Draw(gameTime);
        }

        /// <summary>
        /// This method sets the current screen based on the enum, as well 
        /// as sets nextGameState for whatever is supposed to come next.
        /// </summary>
        /// <param name="gameState"></param>
        /// <returns></returns>
        private Screen LoadScreen(GameState gameState)
        {
            Screen returnScreen = null;

            switch (gameState)
            {
                case GameState.MainMenu:
                    returnScreen =  new DemoScreen(graphics);
                    this.nextGameState = GameState.SpaceInvaders;
                    break;

                case GameState.SpaceInvaders:
                    returnScreen = new GameScreen(graphics);
                    this.nextGameState = GameState.MainMenu;
                    break;

                default:
                    returnScreen = new DemoScreen(graphics);
                    this.nextGameState = GameState.SpaceInvaders;
                    break;
            }

            returnScreen.Initialize();
            returnScreen.LoadContent();
            returnScreen.ReadyToExit = false;
            return returnScreen;
        }
    }
}