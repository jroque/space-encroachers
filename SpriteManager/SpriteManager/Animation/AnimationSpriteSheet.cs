﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

/*
 * More than likely broken right now.
 * Based off of the XNA Spritesheet (Gyroscope) example
 */
namespace SpaceInvaders
{
    class AnimationSpriteSheet : Animation
    {
        private Texture2D Texture;
        public Texture2D GetTexture()   {   return this.Texture;    }

        private Rectangle Frame;
        public Rectangle? GetFrame()    {   return this.Frame;  }

        // Frame Data
        public Point frameSize;
        public Point currentFrame;
        public Point sheetSize;

        StopWatch StopWatch;

        private AnimationSpriteSheet()
        { 
            //If you don't know the resource you want to use, you have bigger problems
        }

        public AnimationSpriteSheet(int MillisecondsPerFrame, String TexturePath)
        {
            this.Texture = TextureManager.getInstance().Add(TexturePath);
            
            // Default for spinning gyroscope
            // Will parameterize this someday!
            this.frameSize      = new Point(75, 75);
            this.currentFrame   = new Point(0, 0);
            this.sheetSize      = new Point(6, 8);

            StopWatch = new StopWatch(MillisecondsPerFrame, this.UpdateFrame);
        }

        public void Update(GameTime gameTime)
        {
            StopWatch.Update(gameTime);
        }

        public void OnTimeUp()
        {
            this.UpdateFrame();
        }

        public void UpdateFrame()
        {
            ++this.currentFrame.X;
            // Advances to the next row on the Sprite Sheet
            if (this.currentFrame.X >= this.sheetSize.X)
            {
                this.currentFrame.X = 0;
                ++this.currentFrame.Y;

                // Loops back to the beginning of the Sprite Sheet
                if (this.currentFrame.Y >= this.sheetSize.Y)
                {
                    this.currentFrame.Y = 0;
                }
            }

            // Rectangle X Y Width Height
            this.Frame.X = currentFrame.X * frameSize.X;
            this.Frame.Y = currentFrame.Y * frameSize.Y;
            this.Frame.Width = frameSize.X;
            this.Frame.Height = frameSize.Y;
        }

        public void Dispose() { }
    }


}