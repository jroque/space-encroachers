﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

/*
 * AnimationImage and AnimationSpriteSheet use this, depending on how the GameObject wants to handle its animation
 */
namespace SpaceInvaders
{
    interface Animation
    {
        void UpdateFrame();
        void Update(GameTime gameTime);
        Texture2D GetTexture();
        Rectangle? GetFrame();
        void Dispose();
    }
}
