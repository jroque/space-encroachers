﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class AnimationImage : Animation
    {
        StopWatch StopWatch;

        SLList<Texture2D> TD_List;
        SLLNode<Texture2D> currentTexture;

        public Texture2D GetTexture()   {   
            return currentTexture.GetData();
        }

        private AnimationImage()
        {
            TD_List = new SLList<Texture2D>();
        }

        // This constructor accepts the resource paths to the different sprites in the animation
        // Each animation has their own FPS.  :)
        public AnimationImage(int MillisecondsPerFrame, params String[] TexturePath) : this()
        {
            if (MillisecondsPerFrame > 0)
            {
                // Create a new StopWatch
                StopWatch = new StopWatch(MillisecondsPerFrame, this.UpdateFrame);
            }
            else
            {
                // Do nothing
            }
            
            foreach (String Path in TexturePath)
            {
                Texture2D newTexture = TextureManager.getInstance().Add(Path);
                this.TD_List.Add(newTexture);
            }

            currentTexture = TD_List.Head;
        }

        public void Update(GameTime gameTime)
        {
            if (StopWatch != null)
            {
                StopWatch.Update(gameTime);
                StopWatch.ResetTimer();
            }
        }

        public void UpdateFrame()
        {
            if (currentTexture.Next != null)
            {
                currentTexture = currentTexture.Next;
            }
            else
            {
                currentTexture = this.TD_List.Head;
            }
            
        }

        // The XNA Draw -- Rectangle parameter is only used for SpriteSheets
        public Rectangle? GetFrame()
        {
            return null;
        }

        public void Dispose()
        {
            //Constants.DebugPrint(this + ".Dispose()");
            SLLNode<Texture2D> TEX_ITERATOR = this.TD_List.Head;

            while (TEX_ITERATOR != null)
            {
                TextureManager.getInstance().Delete(TEX_ITERATOR.GetData());
                this.TD_List.Remove(TEX_ITERATOR.GetData());
                this.currentTexture = null;
                TEX_ITERATOR = TEX_ITERATOR.Next;
            }
        }
    }
}
