﻿/*
 * Simple Linked List class
 */ 
namespace SpaceInvaders.DataStructures
{
    public class SLLNode<T>
    {
        T Data;
        public T GetData() { return this.Data; }

        public SLLNode<T> Next;

        public SLLNode()
        {
            this.Data = default(T);
            this.Next = this;
        }

        public SLLNode(T t)
        {
            this.Data = t;
            this.Next = this;
        }

    }

    public class SLList<T>
    {
        public SLLNode<T> Head;
        public int Length;

        public SLList()
        {
            this.Head = null;
            this.Length = 0;
        }

        // Disallows duplicates
        public T Add(T t)
        {
            if (t != null)
            {
                SLLNode<T> NewNode = new SLLNode<T>(t);

                if (this.Head == null)
                {
                    this.Head = NewNode;
                    this.Head.Next = null;
                }
                else
                {
                    // We want really quick adding, so we'll just add after the head
                    NewNode.Next = this.Head;
                    this.Head = NewNode;
                }

                this.Length++;
            }
            else
            {
                // Do nothing, user submitted null data
            }

            return t;
            
        }

        // Go over this
        public T Get(int index)
        {
            T data = default(T);

            if (this.Head != null)
            {
                if (index < this.Length)
                {
                    SLLNode<T> toGet = this.Head;

                    // Traverse the list
                    for (int i = 0; i < index; i++)
                    {
                        toGet = toGet.Next;
                    }

                    // Assign the data
                    data = toGet.GetData();
                }

            }
            else
            {
                // Do nothing
            }

            return data;
        }

        public bool Contains(T t)
        {
            SLLNode<T> Iterator = this.Head;
            bool result = false;

            while (Iterator != null)
            {
                if (Iterator.GetData().Equals(t))
                {
                    result = true;
                    break;
                }
                Iterator = Iterator.Next;
            }

            return result;
        }

        // Should be OK
        public bool Remove(T t)
        {
            bool result = false;

            // Ensure the list is not blank
            if (this.Head != null)
            {
                if (this.Head.GetData().Equals(t))
                {               
                    // Remove the Head

                    if (this.Head.Next != null)
                    {
                        // Head is NOT the last element on the list
                        this.Head = this.Head.Next;
                    }
                    else
                    {
                        // Head IS the last element on the list
                        this.Head.Next = null;
                        this.Head = null;
                    }

                    --this.Length;
                    result = true;
                }
                else
                {                    
                    // Data is not the head

                    SLLNode<T> NodeBeforeNodeToRemove = this.getPrevNode(t);

                    if (NodeBeforeNodeToRemove.Next != null)
                    {                        
                        NodeBeforeNodeToRemove.Next = NodeBeforeNodeToRemove.Next.Next;

                        --this.Length;

                        result = true;
                    }
                    else
                    {
                        // Do nothing, node not found
                    }
                }
            }
            else
            {
                // Do nothing, list is blank!
            }

            return result;
        }

        // Go over this
        private SLLNode<T> getNode(T t)
        {
            SLLNode<T> NodeToGet = this.Head;
            
            while (NodeToGet != null)
            {
                if (NodeToGet.GetData().Equals(t))
                {
                    break;
                }
                NodeToGet = NodeToGet.Next;
            }

            return NodeToGet;
        }

        // Helper function; Returns the node PREVIOUSLY BEFORE the desired node
        private SLLNode<T> getPrevNode(T t)
        {
            SLLNode<T> NodeToReturn = null;
            SLLNode<T> Iterator = this.Head;

            while (Iterator.Next != null)
            {
                if (Iterator.Next.GetData().Equals(t))
                {
                    NodeToReturn = Iterator;
                    break;
                }
                else
                {
                    // Do nothing
                }

                Iterator = Iterator.Next;
            }

            return Iterator;
        }

        public void RemoveAll()
        {
            if (this.Head != null)
            {
                SLLNode<T> NodeToReturn = this.Head;

                while (NodeToReturn.Next != null)
                {
                    this.Remove(NodeToReturn.GetData());
                    NodeToReturn = NodeToReturn.Next;
                }
            }

        }
    }
}
