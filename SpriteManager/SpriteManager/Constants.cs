﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    static class Constants
    {
        /*
         * Original: 260x224
         *
         * Notes:
         * Top row of aliens is one pixel off (don't care)
         *
         * Left border width is "9"
         * Right border width is "10" --- actually 9
         *
         * Pixels from left AND right at start - 26
         * 
         * Number of ticks to move across the screen - 16
         * Number of ticks until first vertical -- 8 (of course)
         * Alien C width - 12
         * 
         * 2 pixels per move
         * my game - 
         */

        // Helper Random -- Random Number Generators are useful!
        public static Random RANDOM = new Random();

        public static Boolean DEBUG = false;

        // Graphics Dimensions
        // Original Resolution: 260x224
        public const float  SCREEN_SCALE = 2.5f;
        public const int    SCREEN_HEIGHT = (int)(260f * SCREEN_SCALE);
        public const int    SCREEN_WIDTH = (int)(224f * SCREEN_SCALE);

        // Textures are ten times as large as original
        public const float ASSET_SCALE = .1f * SCREEN_SCALE;

        // General configuration iformation
        public static Vector2 STARTING_POSITION = new Vector2(17 * Constants.SCREEN_SCALE, 216 * Constants.SCREEN_SCALE); // Verified
        public static Vector2 LASER_DIRECTION   = new Vector2(0.0f, -10);
        public static Vector2 BOMB_DIRECTION    = new Vector2(0.0f, 3.5f);
        public static Vector2 UFO_SPEED         = new Vector2(2.0f, 0.0f);

        public static Color TOP_COLOR = new Color(254, 30, 30);
        public static Color BOTTOM_COLOR = new Color(30, 254, 30);

        // Point at which missiles blow up
        public static float MISSILE_BARRIER = 36 * Constants.SCREEN_SCALE;
        public static float CLEANUP_TOLERANCE = 100;

        public const float PLAYER_SPEED = 3;
        
        public const int LASER_COOLDOWN = 25; // Milliseconds
        public const int EXPLOSION_DECAY_TIME = 250;
        
        // Audio System Variables
        public static float VOLUME_SFX = 1.0f;
        public static float VOLUME_BGM = 1.0f;
        public static float LISTENER_DISTANCE = 400;

        public static void DebugPrint(String message)
        {
#if DEBUG
            if (Constants.DEBUG) { Console.WriteLine(message); }
#endif
        }
        
    }
}
