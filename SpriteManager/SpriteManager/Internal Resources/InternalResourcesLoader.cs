﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    public static class InternalResourceLoader
    {
        private static ContentManager internalContent;
        public static SpriteFont InternalFont { get; private set; }

        internal static void Init(Game game)
        {
            internalContent = new ResourceContentManager(game.Services, InternalResources.ResourceManager);

            //Load Font
            InternalFont = internalContent.Load<SpriteFont>("Space_Invaders");
        }
    }
}
