﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace SpaceInvaders
{
    /// <summary>
    /// The SoundWave class provides facilties for playing sounds in 2D / 3D.
    /// </summary>
    static class SoundWave
    {
        public static bool Initialized = false;

        public static AudioEngine audioEngine;
        public static WaveBank waveBank;
        public static SoundBank soundBank;
        public static AudioListener listener;        

        public static void Initialize()
        {
            Constants.DebugPrint("SoundWave: Initializing.");

            // Initialize XNA Audio
            audioEngine = new AudioEngine(@"Content\SI_Audio.xgs");
            waveBank    = new WaveBank(audioEngine, @"Content\SI_WaveBank.xwb");
            soundBank   = new SoundBank(audioEngine, @"Content\SI_SoundBank.xsb");

            // Determine the position of the user, and initialize their location to the center of the screen
            listener = new AudioListener();
            listener.Position = new Vector3(((float)Constants.SCREEN_WIDTH / 2.0f), 0, Constants.LISTENER_DISTANCE);

            // Set default volume
            SoundWave.audioEngine.GetCategory("Music").SetVolume(Constants.VOLUME_BGM);
            SoundWave.audioEngine.GetCategory("Default").SetVolume(Constants.VOLUME_SFX);

            SoundWave.Initialized = true;
        }

        public static void Update()
        {
            SoundWave.audioEngine.Update();
        }

        /// <summary>
        /// Calculates the position of the object and plays the sound.  Used for stereo sound effects.
        /// </summary>
        /// <param name="gameObject"></param>
        /// <param name="soundName"></param>
        public static void Play3D(GameObject gameObject, String soundName)
        {
            AudioEmitter emitter = new AudioEmitter();
            emitter.Position = new Vector3(gameObject.Position, 0);

            Cue soundCue = SoundWave.soundBank.GetCue(soundName);
            soundCue.Apply3D(SoundWave.listener, emitter);
            soundCue.Play();
        }

        /// <summary>
        /// Plays the sound in mono, across all channels
        /// </summary>
        /// <param name="soundName"></param>
        public static void Play(String soundName)
        {
            Cue soundCue = SoundWave.soundBank.GetCue(soundName);
            soundCue.Play();
        }
    }
}
