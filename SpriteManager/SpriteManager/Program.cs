using System;
using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {  
            
            using (JGame game = JGame.Instance)
            {
                game.Run();
            }
        }
    }
#endif
}