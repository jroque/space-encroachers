﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using SpaceInvaders.DataStructures;
using System;

namespace SpaceInvaders
{

    class GOManager
    {
        SpriteBatchGroup SBG_Additive;
        SpriteBatchGroup SBG_AlphaBlend;
        SpriteBatchGroup SBG_Opaque;
        SpriteBatchGroup SBG_NonPremultiplied;

        SLList<GameObject> GO_List;
        SLList<SpriteBatchGroup> SBG_List;

        int numObjects;

        #region Singleton Code
        // Singleton Methods
        public static GOManager Instance = new GOManager();
        private GOManager()
        {
            this.SBG_List = new SLList<SpriteBatchGroup>();
            this.GO_List = new SLList<GameObject>();
            this.numObjects = 0;
        }
        #endregion

        /// <summary>
        /// Adds a GameObject to the system
        /// </summary>
        /// <param name="myGameObject"></param>
        public void Add(GameObject myGameObject)
        {
            SpriteBatchGroup mySpriteBatchGroup = this.GetSpriteBatchGroup(myGameObject.BlendState);
            mySpriteBatchGroup.Add(myGameObject);
            GO_List.Add(myGameObject);
            ++this.numObjects;
            //Constants.DebugPrint("GOManager: NumObjects" + this.numObjects);
        }

        /// <summary>
        /// Removes the GameObject from the system.  Also removes the SpriteBatchGroup from the list if it's empty.
        /// </summary>
        /// <param name="myGameObject"></param>
        public void Remove(GameObject myGameObject)
        {
            SpriteBatchGroup mySpriteBatchGroup = this.GetSpriteBatchGroup(myGameObject.BlendState);
            mySpriteBatchGroup.Remove(myGameObject);

            if (this.GO_List.Remove(myGameObject))
            {
                this.numObjects--;

                if (mySpriteBatchGroup.numElements == 0)
                {
                    SBG_List.Remove(mySpriteBatchGroup);
                }
                else { /**/}
#if DEBUG
                Constants.DebugPrint("GOManager: NumObjects" + this.numObjects);
#endif
            }
            else
            {
                // Object not found
                //Constants.DebugPrint("GOManager.Remove() - Object Not Found");
            }

            
        }

        /// <summary>
        /// Private Helper Method - Gets the appropriate Sprite Batch Group
        /// </summary>
        /// <param name="blendState"></param>
        /// <returns></returns>
        private SpriteBatchGroup GetSpriteBatchGroup(BlendState blendState)
        {
            SpriteBatchGroup returnSBG = null;

            if (blendState == BlendState.Additive)
            {
                if (SBG_Additive == null)
                {
                    SBG_Additive = new SpriteBatchGroup(BlendState.Additive);
                    SBG_List.Add(SBG_Additive);
                }
                else { }

                returnSBG = SBG_Additive;

            }
            else if (blendState == BlendState.AlphaBlend)
            {

                if (SBG_AlphaBlend == null)
                {
                    SBG_AlphaBlend = new SpriteBatchGroup(BlendState.AlphaBlend);
                    SBG_List.Add(SBG_AlphaBlend);
                }
                else { }

                returnSBG = SBG_AlphaBlend;

            }
            else if (blendState == BlendState.NonPremultiplied)
            {
                if (SBG_NonPremultiplied == null)
                {
                    SBG_NonPremultiplied = new SpriteBatchGroup(BlendState.NonPremultiplied);
                    SBG_List.Add(SBG_NonPremultiplied);
                }
                else { }

                returnSBG = SBG_NonPremultiplied;

            }
            else if (blendState == BlendState.Opaque)
            {
                if (SBG_Opaque == null)
                {
                    SBG_Opaque = new SpriteBatchGroup(BlendState.Opaque);
                    SBG_List.Add(SBG_Opaque);
                }
                else { }

                returnSBG = SBG_Opaque;
            }
            else
            {
                // Do nothing
            }

            return returnSBG;
        }

        // This handles the update of the individual game objects
        public void Update(GameTime gameTime)
        {            
            for(SLLNode<GameObject> GO_Iterator = GO_List.Head;
                GO_Iterator != null; GO_Iterator = GO_Iterator.Next)
            {
                GameObject currentObject = GO_Iterator.GetData();

                if (!Constants.DEBUG)
                {
                    if (currentObject.Position.Y < 63 * Constants.SCREEN_SCALE)
                    {
                        currentObject.Tint = Constants.TOP_COLOR;
                    }
                    else if (currentObject.Position.Y > 184 * Constants.SCREEN_SCALE)
                    {
                        currentObject.Tint = Constants.BOTTOM_COLOR;
                    }
                    else
                    {
                        currentObject.Tint = Color.White;
                    }
                }   else {  /*Do nothing*/    }

                // Clean up of offscreen objects
                if (currentObject.Position.X > Constants.SCREEN_WIDTH + Constants.CLEANUP_TOLERANCE ||
                    currentObject.Position.Y > Constants.SCREEN_HEIGHT + Constants.CLEANUP_TOLERANCE ||
                    currentObject.Position.Y < 0 - Constants.CLEANUP_TOLERANCE ||
                    currentObject.Position.X < 0 - Constants.CLEANUP_TOLERANCE)
                {
                    currentObject.SetReadyToDispose();
                }

                // Object cleanup -- Immediate
                if (currentObject.GetReadyToDispose())
                {
                    currentObject.Dispose();
                }
                else
                {
                    currentObject.Update(gameTime);
                }
            }
        }


        /// <summary>
        /// Calls all the SpriteBatchGroups to draw all their respective members
        /// </summary>
        public void DrawAll()
        {
            SLLNode<SpriteBatchGroup> SBG_Iterator = SBG_List.Head;

            while (SBG_Iterator != null)
            {
                SBG_Iterator.GetData().Draw();
                SBG_Iterator = SBG_Iterator.Next;
            }
        }

        public void UnloadAll()
        {

            // Clean up Game Objects
            
            SLLNode<GameObject> GO_Iterator = GO_List.Head;
            SLLNode<SpriteBatchGroup> SBG_Iterator = SBG_List.Head;

            while (GO_Iterator != null)
            {
                GameObject myObject = GO_Iterator.GetData();
                myObject.Dispose();

                this.numObjects--;
                //myObjectSBG.Remove(myObject);
                //GO_List.Remove(GO_Iterator.GetData());
                GO_Iterator = GO_Iterator.Next;
            }

            while (SBG_Iterator != null)
            {
                SpriteBatchGroup mySBG = SBG_Iterator.GetData();
                mySBG.UnloadAll();
                this.SBG_List.Remove(mySBG);
                SBG_Iterator = SBG_Iterator.Next;
            }
            
            // I shouldn't need to do this;

            SBG_Additive = null;
            SBG_AlphaBlend = null;
            SBG_Opaque = null;
            SBG_NonPremultiplied = null;
        }

    }
}
