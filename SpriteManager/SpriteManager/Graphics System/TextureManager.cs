﻿using System;

using Microsoft.Xna.Framework.Graphics;
using SpaceInvaders.DataStructures;
using Microsoft.Xna.Framework;

// This is mainly for tracking information, as XNA does not load duplicate content
namespace SpaceInvaders
{


    class TextureManager
    {
        SLList<TextureData> TD_List;

        #region #Singleton Methods
        private static TextureManager instance;
        public static TextureManager getInstance()
        {
            if (TextureManager.instance == null)
            {
                TextureManager.instance = new TextureManager();
            }
            return instance;
        }
        #endregion

        int NumTextures;
        public int GetNumTextures(){return this.NumTextures;}

        int MaxTextures;
        public int GetMaxTextures() { return this.MaxTextures; }

        public TextureManager()
        {
            TD_List = new SLList<TextureData>();
            this.NumTextures = 0;
            this.MaxTextures = 0;
        }

        public Texture2D Add(String TextureResource)
        {
            TextureData TextureDataToAdd = null;
            SLLNode<TextureData> TD_Iterator = TD_List.Head;

            // Check to see if the texture is already loaded
            while (TD_Iterator != null)
            {
                if (TD_Iterator.GetData().GetTextureName().Equals(TextureResource))
                {
                    TextureDataToAdd = TD_Iterator.GetData();
                    break;
                }
                TD_Iterator = TD_Iterator.Next;
            }

            // If the texture was not found within the list
            if (TextureDataToAdd == null)
            {
                //Constants.DebugPrint("TextureManager: Loading " + TextureResource);
                TextureData newTexture = new TextureData(TextureResource);
                this.TD_List.Add(newTexture);

                TextureDataToAdd = newTexture;

                // Update statistics
                ++this.NumTextures;
            }else
            {
                //Constants.DebugPrint("TextureManager: " + TextureResource + " already loaded!");
            }

            TextureDataToAdd.numReferences++;

            if (this.NumTextures > this.MaxTextures)
            {
                this.MaxTextures = this.NumTextures;
            }

            return TextureDataToAdd.GetTexture();
        }

        public void Delete(Texture2D Texture)
        {
            //Constants.DebugPrint("TextureManager: Deleting texture " + Texture);
            SLLNode<TextureData> TD_Iterator = this.TD_List.Head;

            while (TD_Iterator != null)
            {
                TextureData currentData = TD_Iterator.GetData();
                if (currentData.GetTexture().Equals(Texture))
                {
                    if (currentData.numReferences == 1)
                    {
                        //Constants.DebugPrint("TextureManager: Texture no longer in use.  Deleting.");
                        // DISABLED -- Unable to find source of "Disposed" bug.  Tracking all seems accurate.
                        //currentData.Dispose();
                        this.TD_List.Remove(currentData);
                        --this.NumTextures;
                    }
                    else
                    {
                        //Constants.DebugPrint("TextureManager: Texture still in use.  Reducing references.");
                        --currentData.numReferences;
                    }
                }
                TD_Iterator = TD_Iterator.Next;
            }

        }

        public void UnloadAll()
        {
            Constants.DebugPrint("TextureManager: Unloading Textures.  NumTextures: " + this.NumTextures);

            SLLNode<TextureData> TD_Iterator = this.TD_List.Head;

            while (TD_Iterator != null)
            {
                TextureData currentData = TD_Iterator.GetData();
                currentData.Dispose();
                --this.NumTextures;

                TD_Iterator = TD_Iterator.Next;
            }

            Constants.DebugPrint("TextureManager: Unloading complete.");
            Constants.DebugPrint("TextureManager: NumTextures: " + this.NumTextures + " MaxTextures: " + this.MaxTextures);
        }

        class TextureData
        {
            public int numReferences;

            String TextureName;
            public String GetTextureName() { return this.TextureName; }

            Texture2D Texture;
            public Texture2D GetTexture() { return this.Texture; }

            public TextureData()
            {
                this.TextureName = null;
                this.Texture = null;
                this.numReferences = 0;
            }

            // Loads a white box if none specified
            public TextureData(String TextureResource)
                : this()
            {
                if (!TextureResource.Equals(""))
                {
                    // Set the name and load the texture
                    this.TextureName = TextureResource;
                    this.Texture = JGame.Instance.Content.Load<Texture2D>(TextureResource);
                }
                else
                {
                    // Load default texture -- Some pixel
                    this.TextureName = "";
                    this.Texture = new Texture2D(JGame.Instance.graphics.GraphicsDevice, 1, 1);
                    this.Texture.SetData(new[] { Color.White });
                }
            }

            // Releases usage of the texture
            public void Dispose()
            {
                this.Texture.Dispose();
            }
        }
    }
}