﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class SpriteBatchGroup
    {
        SpriteBatch SpriteBatch;
        SLList<GameObject> GOList;        
        BlendState BlendState;
        SpriteSortMode SortMode;

        public int numElements;

        public SpriteBatchGroup(BlendState blendState)
        {
            this.SpriteBatch    = new SpriteBatch(JGame.Instance.GraphicsDevice);
            this.GOList         = new SLList<GameObject>();
            this.BlendState     = blendState;
            this.SortMode       = SpriteSortMode.BackToFront;
        }

        // Add Sprite and Texture...
        public void Add(GameObject Sprite)
        {
            GOList.Add(Sprite);
            Sprite.SpriteBatch = SpriteBatch;
            ++this.numElements;
        }

        public void Remove(GameObject gameObject)
        {
            GOList.Remove(gameObject);
            gameObject.SpriteBatch = null;
            --this.numElements;
        }

        public void Update(GameTime gameTime)
        {
            SLLNode<GameObject> GOIterator = GOList.Head;

            while (GOIterator != null)
            {
                GOIterator.GetData().Update(gameTime);
                GOIterator = GOIterator.Next;
            }
                
        }

        // Default Sort Mode
        public void Draw()
        {
            this.Draw(this.SortMode);
        }

        public void Draw(SpriteSortMode sortMode)
        {
            SpriteBatch.Begin(sortMode, this.BlendState);

            SLLNode<GameObject> GOIterator = GOList.Head;

            while (GOIterator != null)
            {
                // Each object handles its own draw
                GOIterator.GetData().Draw();
                GOIterator = GOIterator.Next;
            }

            SpriteBatch.End();
        }

        public void UnloadAll()
        {
            SLLNode<GameObject> GOIterator = GOList.Head;

            while (GOIterator != null)
            {
                // Each object handles its own draw
                GOList.Remove(GOIterator.GetData());
                GOIterator = GOIterator.Next;
            }
        }

    }
}
