﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

/*
 * Implementation inspired by Dan W and Ekin Bacgel
 */
namespace SpaceInvaders
{
    public delegate void TimerFunction();

    class StopWatch
    {
        TimerFunction callback;
        private int Timer; // in Milliseconds
        private int ResetTime;
        private bool calledBack;
        private bool resetTimer;        

        public StopWatch(int Milliseconds, TimerFunction CallMe)
        {
            this.callback = CallMe;
            this.Timer = Milliseconds;
            this.ResetTime = this.Timer;
            this.calledBack = false;
            this.resetTimer = false;
        }

        public void Update(GameTime gameTime)
        {
            //Constants.DebugPrint("Timer: Updating...");
            if (Timer > 0)
            {
                //Constants.DebugPrint("Timer: Running timer down..." + this.Timer);
                this.Timer -= gameTime.ElapsedGameTime.Milliseconds;
            }
            else
            {
                //Constants.DebugPrint("Timer: Executing callback...");
                if (!calledBack)
                {
                    // Timer has reached 0; Execute callback
                    this.callback();
                    this.calledBack = true;
                    //Constants.DebugPrint("Timer: Callback success");
                }
                else
                {
                    //Constants.DebugPrint("Timer: Callback failed!");
                }

                if (resetTimer)
                {
                    //Constants.DebugPrint("Timer: Resetting Timer");
                    this.Timer = this.ResetTime;
                    this.calledBack = false;
                    this.resetTimer = false;
                }
            }
        }

        // Reset the Timer  This was incorporated into update to ensure that the callback would happen
        // before the timer got reset
        public void ResetTimer()
        {
            this.resetTimer = true;
        }

        // Reset the Timer
        public void ResetTimer(int milliseconds)
        {
            //Constants.DebugPrint("Timer: Setting countdown to " + milliseconds);
            this.ResetTime = milliseconds;
            this.ResetTimer();
        }
    }
}
