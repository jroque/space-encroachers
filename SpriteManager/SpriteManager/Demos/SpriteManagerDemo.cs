﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class SpriteManagerDemo
    {
        GraphicsDeviceManager graphics;
        GOManager SBGManager;

        // SpriteManager Demo Vars
        SLList<GameObject> AlienRow1;
        SLList<GameObject> AlienRow2;
        SLList<GameObject> AlienStack;
        bool Row1Forward = true;
        bool Row2Forward = true;

        public SpriteManagerDemo(GraphicsDeviceManager graphics)
        {
            this.graphics = graphics;
        }

        public void LoadContent()
        {
            SBGManager = GOManager.Instance;

            AlienRow1 = new SLList<GameObject>();
            AlienRow2 = new SLList<GameObject>();
            AlienStack = new SLList<GameObject>();

            SLLNode<GameObject> myIterator;

            SBGManager.Add(new GameObject(new Vector2(0, 0), 128, new AnimationSpriteSheet(1, @"Images\threerings")));
            SBGManager.Add(new GameObject(new Vector2(0, 100), 160, new AnimationSpriteSheet(25, @"Images\threerings")));
            SBGManager.Add(new GameObject(new Vector2(0, 200), 192, new AnimationSpriteSheet(50, @"Images\threerings")));
            SBGManager.Add(new GameObject(new Vector2(0, 300), 255, new AnimationSpriteSheet(100, @"Images\threerings")));

            // Create row 1;

            for (int i = 0; i < 4; i++)
            {
                Alien myAlien = SpaceFactory.CreateAlien(AlienType.A, new Vector2(i * 150 + 100, 0), 64 * (i + 1));
                myAlien.Tint = Color.Red;
                AlienRow1.Add(myAlien);
            }

            myIterator = AlienRow1.Head;

            // Add Row 1 to the SpriteBatch
            float ScaleFactor = 1;
            while (myIterator != null)
            {
                myIterator.GetData().Scale = ScaleFactor;
                SBGManager.Add(myIterator.GetData());
                myIterator = myIterator.Next;

                ScaleFactor *= 1.25f;
            }

            // Create row 2;

            for (int i = 0; i < 4; i++)
            {
                Alien myAlien = SpaceFactory.CreateAlien(AlienType.B, new Vector2(i * 150 + 100, 100), 228);
                myAlien.Tint = Color.Blue;
                AlienRow2.Add(myAlien);
            }

            myIterator = AlienRow2.Head;

            // Add row 2 to the SpriteBatch
            while (myIterator != null)
            {
                myIterator.GetData().Scale = ScaleFactor;
                SBGManager.Add(myIterator.GetData());
                myIterator = myIterator.Next;

                ScaleFactor /= 1.5f;
            }

            // Create row stack

            for (int i = 0; i < 4; i++)
            {
                Alien myAlien = SpaceFactory.CreateAlien(AlienType.C, new Vector2(i * 50 + 100, 200), 192);
                myAlien.Tint = Color.Purple;
                AlienStack.Add(myAlien);
            }

            myIterator = AlienStack.Head;

            // Configure this row
            while (myIterator != null)
            {
                myIterator.GetData().LayerDepth = 0;
                SBGManager.Add(myIterator.GetData());
                myIterator = myIterator.Next;
            }

            SBGManager.Add(SpaceFactory.CreateAlien(AlienType.D, new Vector2(100, 300), 128));
            SBGManager.Add(SpaceFactory.CreateAlien(AlienType.D, new Vector2(250, 300), 128));
            SBGManager.Add(SpaceFactory.CreateAlien(AlienType.D, new Vector2(400, 300), 128));
            SBGManager.Add(SpaceFactory.CreateAlien(AlienType.D, new Vector2(550, 300), 128));

            SBGManager.Add(SpaceFactory.CreateRandomAlien(new Vector2(100, 400), 64));
            SBGManager.Add(SpaceFactory.CreateRandomAlien(new Vector2(250, 400), 64));
            SBGManager.Add(SpaceFactory.CreateRandomAlien(new Vector2(400, 400), 64));
            SBGManager.Add(SpaceFactory.CreateRandomAlien(new Vector2(550, 400), 64));
        }

        public void Update(GameTime gameTime)
        {

            #region SpriteManager Demo Code
            SLLNode<GameObject> myIterator;
            myIterator = AlienRow1.Head;

            #region Row 1
            while (myIterator != null)
            {

                if (Row1Forward)
                {
                    myIterator.GetData().Position += new Vector2(0.0f, 2.0f);
                }
                else
                {
                    myIterator.GetData().Position -= new Vector2(0.0f, 2.0f);
                }

                if ((myIterator.GetData().Position.Y + myIterator.GetData().Height) >= graphics.GraphicsDevice.Viewport.Height
                    || myIterator.GetData().Position.Y <= 0)
                {
                    Row1Forward = !Row1Forward;
                }


                myIterator = myIterator.Next;
            }
            #endregion / SpriteManager Row 12

            #region Row 2
            myIterator = AlienRow2.Head;

            while (myIterator != null)
            {
                if (Row2Forward)
                {
                    myIterator.GetData().Position += new Vector2(2.0f, 0.0f);
                }
                else
                {
                    myIterator.GetData().Position -= new Vector2(2.0f, 0.0f);
                }

                if ((myIterator.GetData().Position.X + myIterator.GetData().Width) >= graphics.GraphicsDevice.Viewport.Width
                    || myIterator.GetData().Position.X <= 0)
                {
                    Row2Forward = !Row2Forward;
                }

                myIterator = myIterator.Next;
            }
            #endregion // SpriteManager Row 2 Demo

            #endregion // SpriteManager Demo Code
            

            
            SBGManager.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            //SBGManager.DrawAll();
        }
    }
}
