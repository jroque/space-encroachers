﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    // Template for Screen
    abstract class Screen
    {
        public bool ReadyToExit;
        public abstract void Initialize();
        public virtual void LoadContent() { }
        public abstract void Dispose();
        public abstract void Update(GameTime gameTime);
        public virtual void Draw(GameTime gameTime)
        {
            GOManager.Instance.DrawAll();
        }

        public virtual void SetReadyToExit()
        {
            this.ReadyToExit = true;
        }
    }
}
