﻿using Microsoft.Xna.Framework; 
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using SpaceInvaders.DataStructures;


namespace SpaceInvaders
{
    class SpaceInvaders
    {
        GraphicsDeviceManager graphics;
        GOManager SBGManager;

        bool canStuffHappen = true;

        Player playerOne;
        SLList<Alien> Aliens;

        public static SLList<SLList<Alien>> ReadyToFire;

        //SLList<GameObject> AliensLeftToRight;
        //SLList<GameObject> AliensRightToLeft;

        HUD myHUD;

        public SpaceInvaders(GraphicsDeviceManager graphics)
        {
            this.graphics = graphics;
            Aliens = new SLList<Alien>();
            ReadyToFire = new SLList<SLList<Alien>>();
        }

        public void Initialize()
        {
            myHUD = new HUD();
            myHUD.Initialize();
        }

        public void LoadContent()
        {
            SBGManager = GOManager.getInstance();

            // Initialize Shields
            Shield myShield = SpaceFactory.CreateShield(new Vector2(0 + 40, Constants.SCREEN_HEIGHT - 200));
            Shield myShield2 = SpaceFactory.CreateShield(new Vector2(125 + 40, Constants.SCREEN_HEIGHT - 200));
            Shield myShield3 = SpaceFactory.CreateShield(new Vector2(250 + 40, Constants.SCREEN_HEIGHT - 200));
            Shield myShield4 = SpaceFactory.CreateShield(new Vector2(375 + 40, Constants.SCREEN_HEIGHT - 200));

            int numRows = 5;
            int numColumns = 11;

            // Create 11 lists, one for each column
            for (int i = 0; i < numColumns; i++)
            {
                SpaceInvaders.ReadyToFire.Add(new SLList<Alien>());
            }

            AlienType alienType = AlienType.A;

            for (int i = 0; i < numRows; i++)
            {
                switch (i)
                {
                    case 0:
                        alienType = AlienType.B;
                        break;
                    case 1:
                        alienType = AlienType.A;
                        break;
                    case 2:
                        alienType = AlienType.A;
                        break;
                    case 3:
                        alienType = AlienType.C;
                        break;
                    case 4:
                        alienType = AlienType.C;
                        break;                    
                }

                for (int j = 0; j < numColumns; j++)
                {
                    float topLeftX = 20;
                    float topLeftY = 80;
                    Alien myAlien = SpaceFactory.CreateAlien(alienType, new Vector2(topLeftX + j * 40, topLeftY + i * 40), 255);

                    ReadyToFire.Get(j).Add(myAlien);

                    Aliens.Add(myAlien);
                }
            }

            playerOne = new Player();
        }

        public void Update(GameTime gameTime)
        {
            playerOne.Update(gameTime);

            SLLNode<SLList<Alien>> L_Iterator = ReadyToFire.Head;

            while (L_Iterator != null)
            {
                SLList<Alien> myList = L_Iterator.GetData();
                if (myList.Head != null)
                {
                    myList.Head.GetData().Tint = Color.Red;
                    myList.Head.GetData().ReadytoFire = true;

                }

                L_Iterator = L_Iterator.Next;
            }

            if (gameTime.TotalGameTime.Seconds % 2 == 0)
            {
                if (canStuffHappen)
                {
                    SLLNode<Alien> GO_Iterator = Aliens.Head;
                    bool changeDirection = false;


                    while (GO_Iterator != null)
                    {
                        Alien currentAlien = GO_Iterator.GetData();

                        if (currentAlien.ReadytoFire)
                        {
                            ((Alien)currentAlien).Fire();     
                        }

                        currentAlien.Position += GameData.ALIEN_DIRECTION_NEXT;
         
                        if (currentAlien.Position.X + currentAlien.Width >= Constants.SCREEN_WIDTH ||
                            currentAlien.Position.X <= 0)
                        {
                            changeDirection = true;
                        }
                        
                        GO_Iterator = GO_Iterator.Next;
                    }

                    if (changeDirection)
                    {
                        GameData.ALIEN_DIRECTION_HORIZONTAL = -GameData.ALIEN_DIRECTION_HORIZONTAL;
                        GameData.ALIEN_DIRECTION_NEXT = GameData.ALIEN_DIRECTION_HORIZONTAL;
                    }

                    SoundWave.Play("Pulse_1");
                    canStuffHappen = false;
                }
            }
            else
            {
                canStuffHappen = true;
            }

            // Update position
            SBGManager.Update(gameTime);

            // Update collisions
            CollisionManager.getInstance().Update(gameTime);

            // Update HUD
            myHUD.Update(gameTime);
        }

        public void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);
            SBGManager.DrawAll();
            myHUD.Draw(gameTime);
        }
    }
}
