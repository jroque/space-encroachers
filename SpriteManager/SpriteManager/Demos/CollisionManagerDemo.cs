﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;


namespace SpaceInvaders
{
    class CollisionManagerDemo
    {
        GraphicsDeviceManager graphics;
        GOManager SBGManager;        

        CollisionManager myCM = CollisionManager.Instance;

        bool canStuffHappen = true;

        Player playerOne;

        public CollisionManagerDemo(GraphicsDeviceManager graphics)
        {
            this.graphics = graphics;
        }

        public void Initialize()
        {
            SBGManager = GOManager.Instance;
            playerOne = new Player();
        }


        public void Update(GameTime gameTime)
        {

            playerOne.Update(gameTime);

            if (gameTime.TotalGameTime.Seconds % 2 == 0)
            {
                if (canStuffHappen)
                {

                    Alien Alien1 = SpaceFactory.CreateRandomAlien(new Vector2(-100, 100), 255);
                    Alien Alien2 = SpaceFactory.CreateRandomAlien(new Vector2(640, 300), 255);
                    Alien Alien3 = SpaceFactory.CreateRandomAlien(new Vector2(-100, 100), 255);
                    Alien Alien4 = SpaceFactory.CreateRandomAlien(new Vector2(640, 300), 255);

                    Vector2 moveLeft = new Vector2(-8, 0);
                    Vector2 moveRight = new Vector2(4, 0);
                    Vector2 moveUp = new Vector2(-3, 1);
                    Vector2 moveDown = new Vector2(4, 2);

                    Alien1.Direction = moveRight;
                    Alien2.Direction = moveLeft;
                    Alien3.Direction = moveDown; 
                    Alien4.Direction = moveUp;

                    canStuffHappen = false;

                }
            }
            else
            {
                canStuffHappen = true;
            }

            SBGManager.Update(gameTime);
            myCM.Update(gameTime);
        }
    }
}
