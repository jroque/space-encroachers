﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace SpaceInvaders
{
    class DemoScreen : Screen
    {
        SpriteBatch spriteBatch;
        Player player;

        public DemoScreen(GraphicsDeviceManager graphics)
        {
            spriteBatch = new SpriteBatch(JGame.Instance.GraphicsDevice);            
            player = new Player();
        }

        public override void Initialize()
        {
            this.ReadyToExit = false;
            GameData.GAME_OVER = false;                        
            HUD.Instance.drawLives = false;
            HUD.Instance.drawP2Score = true;
        }

        public override void LoadContent()
        {
        }


        public override void Dispose()
        {
            CollisionManager.Instance.UnloadAll();
            GOManager.Instance.UnloadAll();
            //TextureManager.getInstance().UnloadAll();
        }
        public override void Update(GameTime gameTime) {

            if (Input.IsKeyDown(Keys.F12, true))
            {
                JGame.Instance.Exit();
            }
            else { /**/}
            

            if (GameData.NUM_CREDITS > 0)
            {
                if(Input.IsKeyDown(Keys.D1, true))
                {
                    GameData.PLAYER_MODE = PlayerMode.OnePlayer;
                    GameData.NUM_CREDITS--;
                    this.ReadyToExit = true;

                }

                if (GameData.NUM_CREDITS > 1)
                {
                    if (Input.IsKeyDown(Keys.D2, true))
                    {
                        GameData.PLAYER_MODE = PlayerMode.TwoPlayer;
                        GameData.NUM_CREDITS -= 2;
                        this.ReadyToExit = true;
                    }
                }
            }

            player.Update(gameTime);
            GOManager.Instance.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {

            if (!this.ReadyToExit)
            {
                spriteBatch.Begin();

                if (GameData.NUM_CREDITS == 1)
                {
                    spriteBatch.DrawString(InternalResourceLoader.InternalFont, "         PUSH\n\nONLY 1PLAYER BUTTON",
                        new Vector2(Constants.SCREEN_WIDTH / 2 - 185,
                        Constants.SCREEN_HEIGHT / 2 - 150), Color.White, 0, Vector2.Zero,
                        1.0f, SpriteEffects.None, 0);

                }
                else if (GameData.NUM_CREDITS > 1)
                {
                    spriteBatch.DrawString(InternalResourceLoader.InternalFont, "         PUSH\n\n1 OR 2PLAYERS BUTTON",
                        new Vector2(Constants.SCREEN_WIDTH / 2 - 185,
                        Constants.SCREEN_HEIGHT / 2 - 150), Color.White, 0, Vector2.Zero,
                        1.0f, SpriteEffects.None, 0);
                }
                else
                {
                    string Title = "SPACE ENCROACHERS";
                    string Controls =
                        "A JEREMIAH ROQUE IMITATION\n" +
                        "FOR DEPAUL SE511\n\n" +
                        "INSTRUCTIONS:\n" +
                        "Destroy all the encroachers!\n\n" +
                        "CONTROLS:\n" +
                        "Insert Coin...........Tab\n" +
                        "P1............................1\n" +
                        "P2............................1\n" +
                        "Move Left..............Left Arrow\n" +
                        "Move Right.............Right Arrow\n" +
                        "Fire........................Space\n" +
                        "Debug Mode............F1\n" +
                        "Toggle BGM............F3\n" +
                        "Toggle SFX............F4\n" +
                        "PAUSE.....................P\n" +
                         "Quit........................F12";

                    spriteBatch.DrawString(InternalResourceLoader.InternalFont, Title,
                        new Vector2(10 * Constants.SCREEN_SCALE,
                        128), Color.White, 0, Vector2.Zero,
                        1.0f, SpriteEffects.None, 0);

                    spriteBatch.DrawString(InternalResourceLoader.InternalFont, Controls,
                        new Vector2(10 * Constants.SCREEN_SCALE,
                        160), Color.White, 0, Vector2.Zero,
                        0.75f, SpriteEffects.None, 0);

                }

                spriteBatch.End();
                GOManager.Instance.DrawAll();
            }
            base.Draw(gameTime);
        }
    }
}
