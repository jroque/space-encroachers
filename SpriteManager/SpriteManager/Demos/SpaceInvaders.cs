﻿using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;
using System;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;


namespace SpaceInvaders
{
    class GameScreen : Screen
    {
        private enum GameState
        {
            Loading,
            InGame
        }

        GOManager GOManager = GOManager.Instance;
        
        public static Player ThePlayer;
        String currentCue;
        StopWatch ReadyTimer;   // When the initial message displays
        StopWatch TickTimer;    // When the aliens move
        StopWatch UFOTimer;     // When the UFOs appear
        StopWatch RespawnTimer; // When the ship respawns
        StopWatch QuitTimer;    // When "Game Over" reverts back to the other screen

        GameState State;
        SpriteBatch TextBatch;

        bool PAUSE;

        /// <summary>
        /// Called only when loading the game
        /// </summary>
        /// <param name="graphics"></param>
        public GameScreen(GraphicsDeviceManager graphics)
        {
            Constants.DebugPrint("Loading Space Invaders...");

            GameData.ResetScoresAll();
            GameData.CURRENT_LEVEL = 1;
            HUD.Instance.drawLives = true;
            GameData.GAME_OVER = false;
            PAUSE = false;
            TextBatch = new SpriteBatch(JGame.Instance.GraphicsDevice);

            GameData.P1 = new PlayerData();
            GameData.P1.Initialize();

            if (GameData.PLAYER_MODE == PlayerMode.TwoPlayer)
            {
                GameData.P2 = new PlayerData();

                GameData.P1.OtherPlayer = GameData.P2;
                GameData.P2.OtherPlayer = GameData.P1;

                GameData.P2.Initialize();
                GameData.P2.Hide();

            }
            else
            {
                HUD.Instance.drawP2Score = false;
            }

            GameData.SetCurrentPlayer(GameData.P1);
            
            this.ReadyTimer = new StopWatch(2000, this.Ready);
            this.TickTimer = new StopWatch(this.TickLength(), this.Tick);
            this.UFOTimer = new StopWatch(10000, this.UFO);
            this.RespawnTimer = new StopWatch(3000, this.SpawnPlayer);
            this.QuitTimer = new StopWatch(3000, this.SetReadyToExit);

            this.ReadyToExit = false;
            this.State = GameState.Loading;
        }

        #region Initialization
        /// <summary>
        /// In this case, I am using initialize to begin a new level
        /// </summary>
        public override void Initialize()
        {            
            Constants.DebugPrint("SpaceInvaders: Initializing...");
            
            // Initialize facilities
            this.ReadyTimer.ResetTimer();
            this.TickTimer.ResetTimer();
            this.UFOTimer.ResetTimer();
            this.RespawnTimer.ResetTimer();
            this.QuitTimer.ResetTimer();
            this.currentCue     = "Pulse_2";

            if (GameData.ALIEN_DIR_HORIZONTAL.X < 0)
            {
                GameData.ALIEN_DIR_HORIZONTAL.X = -GameData.ALIEN_DIR_HORIZONTAL.X;
            }

            GameScreen.ThePlayer  = new Player();
            
            // Load data to prevent loading during game
            SpaceFactory.CreateBombA(new Vector2(-100, -100)).GetReadyToDispose();
            SpaceFactory.CreateBombB(new Vector2(-100, -100)).GetReadyToDispose();
        }

        #endregion        

        public override void Update(GameTime gameTime)
        {
            if(Input.IsKeyDown(Keys.F12, true))
            {
                this.ReadyToExit = true;
            }
            if (Input.IsKeyDown(Keys.F2, true))
            {
                this.PAUSE = !this.PAUSE;
            }

            if (Constants.DEBUG)
            {
                // Debug to test player switching
                if (Input.IsKeyDown(Keys.F6, true))
                {
                    GameData.SwitchPlayer();
                }
                else { /**/}
            }
            else { /**/}

       

            if (this.State == GameState.Loading)
            {
                // Do Loading Stuff
                ReadyTimer.Update(gameTime);
            }
            else
            {
                if (this.PAUSE) { }
                else
                {
                    if (GameData.CURR_PLAYER.NUM_LIVES == 0)
                    {
                        GameData.CURR_PLAYER.GAME_OVER = true;
                    }

                    // Evaluate overall Conditions
                    if (GameData.PLAYER_MODE == PlayerMode.OnePlayer)
                    {
                        GameData.GAME_OVER = GameData.P1.GAME_OVER;
                    }
                    else if (GameData.PLAYER_MODE == PlayerMode.TwoPlayer)
                    {
                        GameData.GAME_OVER = GameData.P1.GAME_OVER && GameData.P2.GAME_OVER;
                    }
                    else { /**/}



                    if (!GameData.GAME_OVER)
                    {

                        #region ReadyToFire Debug
                        // Signal the aliens that are ready to fire
                        if (Constants.DEBUG)
                        {
                            SLLNode<SLList<Alien>> column_iterator = GameData.CURR_PLAYER.Aliens.Head;

                            while (column_iterator != null)
                            {
                                SLList<Alien> myList = column_iterator.GetData();
                                if (myList.Head != null)
                                {
                                    myList.Head.GetData().Tint = Color.Red;
                                }
                                column_iterator = column_iterator.Next;
                            }

                        }
                        #endregion

                        if (GameData.CURR_PLAYER.NUM_ALIENS > 0)
                        {
                            if (ThePlayer.ship.alive)
                            {
                                ThePlayer.Update(gameTime);                   
                                TickTimer.Update(gameTime);


                                if (GameData.CURR_PLAYER.NUM_ALIENS > 7)
                                {
                                    UFOTimer.Update(gameTime);
                                }
                                
                                // Aliens attack when needed
                                if (GameData.NUM_BOMBS_ON_SCREEN == 0)
                                {
                                    this.AliensAttack();
                                }
                            }
                            else
                            {
                                RespawnTimer.Update(gameTime);
                            }
                        }
                        else
                        {
                            // Initialize the next level
                            GameData.CURR_PLAYER.DisposeAll();
                            GameData.CURR_PLAYER.Initialize();
                            GameData.CURR_PLAYER.CURRENT_LEVEL++;
                        }
                    }
                    else
                    {
                        this.QuitTimer.Update(gameTime);
                    }

                    GOManager.Update(gameTime);
                    CollisionManager.Instance.Update(gameTime);

                }// Pause
            }// InGame
        }// Update

        public override void Draw(GameTime gameTime)
        {
            if (this.State == GameState.Loading)
            {
                TextBatch.Begin();

                String ReadyMessage = null;

                if (GameData.CURR_PLAYER == GameData.P1)
                {
                    ReadyMessage = "PLAY PLAYER<1>";
                }
                else
                {
                    ReadyMessage = "PLAY PLAYER<2>";
                }
                
                TextBatch.DrawString(InternalResourceLoader.InternalFont, ReadyMessage,
                new Vector2(57 * Constants.SCREEN_SCALE, 113 * Constants.SCREEN_SCALE), 
                Color.White, 0, Vector2.Zero,   1.0f, SpriteEffects.None, 0);

                TextBatch.End();
            }
            else if (this.State == GameState.InGame)
            {
                if (this.PAUSE)
                {
                    TextBatch.Begin();

                    TextBatch.DrawString(InternalResourceLoader.InternalFont,
                        "PAUSED", new Vector2(88 * Constants.SCREEN_SCALE, 50 * Constants.SCREEN_SCALE),
                        Constants.TOP_COLOR, 0, Vector2.Zero,
                            1.0f, SpriteEffects.None, 0);
                    TextBatch.End();
                    
                }

                if (GameData.PLAYER_MODE == PlayerMode.TwoPlayer && GameData.CURR_PLAYER.GAME_OVER)
                {
                    String GameOverMessage = null;

                    if (GameData.CURR_PLAYER == GameData.P1)
                    {
                        GameOverMessage = "GAME  OVER  PLAYER<1>";
                    }
                    else
                    {
                        GameOverMessage = "GAME  OVER  PLAYER<2>";
                    }

                    TextBatch.Begin();
                    TextBatch.DrawString(InternalResourceLoader.InternalFont, GameOverMessage,
                        new Vector2(33 * Constants.SCREEN_SCALE, 225 * Constants.SCREEN_SCALE),
                        Constants.BOTTOM_COLOR, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0);
                    TextBatch.End();
                }
                else { /**/}
                GOManager.Instance.DrawAll();
            }
        }

        #region TIMER CALLBACKS
        // Readies the game
        private void Ready()
        {
            State = GameState.InGame;
            ReadyTimer.ResetTimer();
        }

        /// <summary>
        /// Respawns the ship
        /// </summary>
        public void SpawnPlayer()
        {
            if (GameData.PLAYER_MODE == PlayerMode.TwoPlayer)
            {
                if (GameData.CURR_PLAYER.OtherPlayer.NUM_LIVES > 0)
                {
                    this.State = GameState.Loading;
                    GameData.SwitchPlayer();
                }
                else
                {
                }
            }
            else
            {

            }

            // Single player or one player remaining.  Respawn ship
            GameScreen.ThePlayer.ship.Position = Constants.STARTING_POSITION;
            GameScreen.ThePlayer.ship.Respawn();
            this.RespawnTimer.ResetTimer();
        }

        /// <summary>
        /// Generates a UFO
        /// </summary>
        private void UFO()
        {
            Constants.DebugPrint("SpaceInvaders: A wild UFO appears!");

            if (Constants.RANDOM.Next(2) % 2 == 0)
            {
                UFO myUFO = SpaceFactory.CreateUFO(new Vector2(20, 100));
                myUFO.Direction = Constants.UFO_SPEED;
            }
            else
            {
                UFO myUFO = SpaceFactory.CreateUFO(new Vector2(Constants.SCREEN_WIDTH, 100));
                myUFO.Direction = -Constants.UFO_SPEED;
            }
            
            UFOTimer.ResetTimer(Constants.RANDOM.Next(10000, 30000));
        }

        /// <summary>
        /// Causes a random alien to fire, chosen from the bottom row.
        /// </summary>
        private void AliensAttack()
        {
            GameData.CURR_PLAYER.AlienFire();
        }
        #endregion

        #region TICK
        /// <summary>
        /// Moves all the aliens
        /// </summary>
        private void Tick()
        {
            GameData.CURR_PLAYER.AlienMove();
            this.TickSound();       

            // Set the timer based on how many aliens there are
            TickTimer.ResetTimer(this.TickLength());            
        }

        /// <summary>
        /// Calculates the time that aliens are going to move
        /// in milliseconds.  Measured the original game and
        /// tried to find an equation of best fit
        /// </summary>
        /// <returns></returns>
        private int TickLength()
        {
            return (int)((166.37 * Math.Log(GameData.CURR_PLAYER.NUM_ALIENS) - 0.7683) + 100
                - GameData.CURR_PLAYER.CURRENT_LEVEL * 100);
        }

        /// <summary>
        /// 
        /// </summary>
        private void TickSound()
        {            
            SoundWave.Play(currentCue);
            
            if(currentCue.Equals("Pulse_1"))
            {
                currentCue = "Pulse_2";
            }
            else if(currentCue.Equals("Pulse_2"))
            {
                currentCue = "Pulse_3";
            }
            else if(currentCue.Equals("Pulse_3"))
            {
                currentCue = "Pulse_4";
            }
            else if (currentCue.Equals("Pulse_4"))
            {
                currentCue = "Pulse_1";
            }
        }
        #endregion

        public override void Dispose()
        {
            GOManager.Instance.UnloadAll();
            CollisionManager.Instance.UnloadAll();            
        }
    }
}