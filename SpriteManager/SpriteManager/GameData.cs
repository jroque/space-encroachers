﻿using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    public enum PlayerMode
    {
        OnePlayer,
        TwoPlayer
    }

    public enum AlienDirection
    {
        Vertical,
        Horizontal
    }

    /// <summary>
    /// Rather than overload SpaceInvaders with a bajillion variables,
    /// I've decided to store all that data in a separate class
    /// for the sake of cleanliness
    /// </summary>
 
    class GameData
    {
        public static PlayerMode PLAYER_MODE;
        public static PlayerData P1;
        public static PlayerData P2;
        public static PlayerData CURR_PLAYER = new PlayerData(); // Initialize to P1
        public static void SetCurrentPlayer(PlayerData p) { GameData.CURR_PLAYER = p; }
        public static PlayerData GetCurrentPlayer() { return GameData.CURR_PLAYER; }

        public static int P1_SCORE = 0;
        public static int P2_SCORE = 0;
        public static int NUM_CREDITS = 0;
        public static int HI_SCORE = 255;
        public static int NUM_MISSILES_ON_SCREEN = 0;
        public static int NUM_BOMBS_ON_SCREEN = 0;
        public static int NUM_ALIENS = 0;
        public static int CURRENT_LEVEL = 0;
        public static int LEVEL_OFFSET = (int)(24 * Constants.SCREEN_SCALE);

        // Rows and columns for aliens
        public static int numRows = 5;
        public static int numColumns = 11;
        public const  int ALIEN_FIRE_RATE = 750; // In milliseconds

        public static int VERTICAL_THRESHOLD = (int)(220 * Constants.SCREEN_SCALE); // Original: 224
        public static bool GAME_OVER = false;

        public const int ALIEN_LEFT_BORDER = (int)(9 * Constants.SCREEN_SCALE);
        public const int ALIEN_RIGHT_BORDER = (int)(Constants.SCREEN_WIDTH - (9 * Constants.SCREEN_SCALE));

        public static bool P1_1UP = false;
        public static bool P2_1UP = false;

        // Aliens move "8" pixels when they go vertical.
        // 2 Pixels horizontal
        // Starting position (26, 64)
        // Level 2 Starting Position (26, 88)
        // Level 3 Starting Position 
        public static Vector2 ALIEN_START_POS = new Vector2(26 * Constants.SCREEN_SCALE, 64 * Constants.SCREEN_SCALE);
        public static Vector2 ALIEN_DIR_HORIZONTAL = new Vector2(2 * Constants.SCREEN_SCALE, 0);
        public static Vector2 ALIEN_DIR_VERTICAL = new Vector2(0, 8 * Constants.SCREEN_SCALE);

        public static void ResetScoresAll()
        {
            ResetScoreP1();
            ResetScoreP2();            
        }

        public static void ResetScoreP1()
        {
            P1_SCORE = 0;
        }
        public static void ResetScoreP2()
        {
            P2_SCORE = 0;
        }

        public static void ScoreIncrement(int points)
        {
            if (GameData.CURR_PLAYER == GameData.P1)
            {
                GameData.P1_SCORE += points;
                if (GameData.P1_SCORE > GameData.HI_SCORE)
                {
                    GameData.HI_SCORE = GameData.P1_SCORE;
                }

                if (GameData.P1_SCORE >= 1500 && !GameData.P1_1UP)
                {
                    GameData.P1.NUM_LIVES++;
                    GameData.P1_1UP = true;
                    SoundWave.Play("SMW_1up");                    
                }
            }
            else
            {
                GameData.P2_SCORE += points;
                if (GameData.P2_SCORE > GameData.HI_SCORE)
                {
                    GameData.HI_SCORE = GameData.P2_SCORE;
                }

                if (GameData.P2_SCORE >= 1500 && !GameData.P2_1UP)
                {
                    GameData.P2.NUM_LIVES++;
                    GameData.P2_1UP = true;
                    SoundWave.Play("SMW_1up");
                }
                
            }

        }

        public static void SwitchPlayer()
        {
            if (GameData.PLAYER_MODE == PlayerMode.TwoPlayer)
            {
                GameData.CURR_PLAYER.Hide();

                // Currently P1
                if (GameData.CURR_PLAYER == GameData.P1)
                {
                    GameData.CURR_PLAYER = GameData.P2;

                }
                else // Are P2
                {
                    // P2 Switch to P1
                    GameData.CURR_PLAYER = GameData.P1;
                }

                GameData.CURR_PLAYER.Show();
            }

        }
    }
}
