﻿using Microsoft.Xna.Framework;

using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class CollisionManager
    {
        SLList<Collidable> Collidables;
        public int numObjects
        {
            get
            {
                return Collidables.Length;
            }
        }

        #region #Singleton Methods
        private CollisionManager()
        {
            Collidables = new SLList<Collidable>();
        }
        public static CollisionManager Instance = new CollisionManager();
        #endregion

        /// <summary>
        /// Adds objects to the list of things that will be collided with one another
        /// Will NOT add duplicate items
        /// Returns true 
        /// </summary>
        /// <param name="myObject"></param>
        public bool Add(Collidable myObject)
        {
            bool result = false;
            if (!this.Collidables.Contains(myObject))
            {
                this.Collidables.Add(myObject);
            }
            else
            {
                // Do nothing
            }

            return result;
        }

        /// <summary>
        /// Returns true if the remove was successful
        /// </summary>
        /// <param name="myCollidable"></param>
        public bool Remove(Collidable myCollidable)
        {
            return this.Collidables.Remove(myCollidable);
        }

        public bool Contains(Collidable c)
        {
            return this.Collidables.Contains(c);
        }


        public void Update(GameTime gameTime)
        {
            SLLNode<Collidable> Collider = Collidables.Head;

            while (Collider != null)
            {
                // For each collider
                SLLNode<Collidable> Collidee = Collidables.Head;

                // Check its collision with each collidee
                while (Collidee != null)
                {
                    if (Collider != Collidee)
                    {
                        Collider.GetData().checkCollisions(Collidee.GetData());
                    }
                    else
                    {
                        // Do nothing -- Don't want to register things colliding w/self
                    }
                    // Advance to the next node
                    Collidee = Collidee.Next;
                }

                // Advance to the next node
                Collider = Collider.Next;
            }
        }

        public void UnloadAll()
        {
            // Clean up Game Objects

            SLLNode<Collidable> GO_Iterator = Collidables.Head;

            while (GO_Iterator != null)
            {
                Collidable myCollidable = GO_Iterator.GetData();
                this.Remove(myCollidable);
                GO_Iterator = GO_Iterator.Next;
            }
        }
    }

}