﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    abstract class Collidable : GameObject
    {
        public CollisionBox collisionBox;
        bool isCollided = false;

        public new float Scale {
            get
            {
                return this.Scale;
            }
            set
            {
                base.Scale = value;
                this.collisionBox.Width = (int)(value * this.collisionBox.Width);
                this.collisionBox.Height = (int)(value * this.collisionBox.Height);
                this.debugRectangle.Height = (int)(value * this.debugRectangle.Height);
                this.debugRectangle.Width = (int)(value * this.debugRectangle.Width);
            }
        }

        // Debug data
        Texture2D debugTexture;
        Rectangle debugRectangle;
        Color debugColor;

        public Collidable() : base() { }

        public Collidable(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            this.collisionBox = new CollisionBox(this.Position, this.Width, this.Height);

            debugTexture = new Texture2D(JGame.Instance.graphics.GraphicsDevice, 1, 1);
            debugTexture.SetData(new[] { Color.White });
            debugRectangle = new Rectangle((int)this.collisionBox.TopLeft.X, (int)this.collisionBox.TopLeft.Y, (int)this.collisionBox.Width, (int)this.collisionBox.Height);
        }

        public abstract void Collision(Collidable other);

        #region Collide() Templates
        public virtual void Collide(Ship s)
        {
            //if(Constants.DEBUG){Constants.DebugPrint("Ship Collision not implemented");}
        }
        public virtual void Collide(Alien a)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("Alien Collision not implemented"); }
        }
        public virtual void Collide(Missile m)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("Missile Collision not implemented"); }
        }
        public virtual void Collide(UFO u)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("UFO Collision not implemented"); }
        }
        public virtual void Collide(Bomb b)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("Bomb Collision not implemented"); }
        }
        public virtual void Collide(Shield s)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("Shield Collision not implemented"); }
        }
        public virtual void Collide(ShieldPixel s)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("SubShield Collision not implemented"); }
        }
        public virtual void Collide(ImpactPixel ip)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("ImpactPixels Collision not implemented"); }
        }
        public virtual void Collide(MissileImpact ip)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("MissileImpact Collision not implemented"); }
        }
        public virtual void Collide(BombImpact bp)
        {
            //if (Constants.DEBUG) { Constants.DebugPrint("BombImpact Collision not implemented"); }
        }
        #endregion

        /// <summary>
        /// This method checks the other box's top and bottom against the current top/bottom,
        /// then afterwards checks the left/right against the current left/right.
        /// Returns true if and only if a corner is actually inside the box.
        /// </summary>
        /// <param name="otherBox"></param>
        /// <returns></returns>
        public virtual void checkCollisions(Collidable other)
        {
            CollisionBox otherBox = other.collisionBox;
            bool collided = this.checkCollisionBox(otherBox);

            if (collided)
            {
                this.Collision(other);
                this.isCollided = true;
            }
            else
            {
                this.isCollided = false;
            }
        }

        /// <summary>
        /// Helper method to calculate whether or not two boxes are intersecting
        /// </summary>
        /// <param name="otherBox"></param>
        /// <returns></returns>
        private bool checkCollisionBox(CollisionBox otherBox)
        {
            CollisionBox myBox = this.collisionBox;

            // It's easier to disprove this than to prove it

            bool result = true;
            if (otherBox.Bottom < myBox.Top)
            {
                // Box is above
                result = false;
            }
            else if (otherBox.Left > myBox.Right)
            {
                // Box is to the right
                result = false;
            }
            else if (otherBox.Top > myBox.Bottom)
            {
                // Box is below
                result = false;
            }
            else if (otherBox.Right < myBox.Left)
            {
                // Box is to the left
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Adds the object back to the collision manager
        /// Also adds the object back to the GameObject Manager
        /// </summary>
        public override void Show()
        {
            CollisionManager.Instance.Add(this);
            base.Show();
        }

        /// <summary>
        /// Removes the object from the CollisionManager and GameObject Manager
        /// </summary>
        public override void Hide()
        {
            CollisionManager.Instance.Remove(this);
            base.Hide();
        }

        public override void Update(GameTime gameTime)
        {

            this.collisionBox.setPosition(this.Position);

            if (Constants.DEBUG)
            {
                this.debugRectangle.X = (int)this.Position.X;
                this.debugRectangle.Y = (int)this.Position.Y;
            }
            else
            {
                // Do nothing
            }

            base.Update(gameTime);
        }

        public override void Draw()
        {

            if (Constants.DEBUG)
            {
                Color myColor;

                if (this.isCollided)    {   myColor = Color.Red;    }
                else
                {
                    myColor = Color.Green;    
                }

                debugColor.R = myColor.R;
                debugColor.G = myColor.G;
                debugColor.B = myColor.B;
                debugColor.A = 8;
                
                this.SpriteBatch.Draw(this.debugTexture, this.debugRectangle, debugColor);
            }else{  /* Do nothing, debug is off*/   }

            base.Draw();
        }

        /// <summary>
        /// Called when the object is to no longer be used.
        /// Removes itself from the CollisionManager and calls GameObject.Dispose()
        /// </summary>
        public override void Dispose()
        {
            if (CollisionManager.Instance.Contains(this))
            {
                CollisionManager.Instance.Remove(this);
            }
            else
            {
                // Do nothing
            }

            base.Dispose();
        }
    }
}
