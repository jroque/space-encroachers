﻿using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class CollisionBox
    {
        public Vector2 TopLeft;
        public Vector2 BottomRight;

        public float Width;
        public float Height;

        public float Left
        {  
            get {    return TopLeft.X;   }   
        }

        public float Right
        {
            get {   return BottomRight.X;  }
        }

        public float Top
        {
            get { return TopLeft.Y; }
        }

        public float Bottom
        {
            get { return BottomRight.Y;  }
        }

        private CollisionBox(Vector2 Position){}

        public CollisionBox(Vector2 Position, float width, float height)
        {
            this.setPosition(Position);
            this.Width = width;
            this.Height = height;
        }

        public void setPosition(Vector2 inPosition)
        {
            this.TopLeft = inPosition;
            this.BottomRight.X = this.TopLeft.X + Width;
            this.BottomRight.Y = this.TopLeft.Y + Height;
        }

    }
}