﻿using SpaceInvaders.DataStructures;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    abstract class CollisionGroup : Collidable
    {
        protected SLList<Collidable> Collidables;
        
        public CollisionGroup(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation) { }


        protected void checkChildren(Collidable other)
        {
            for(SLLNode<Collidable> COIterator = Collidables.Head;
                COIterator != null; COIterator = COIterator.Next)
            {
                COIterator.GetData().checkCollisions(other);
            }
        }

        // Add the object
        public void Add(Collidable other)
        {
            this.Collidables.Add(other);
        }

        // Remove the object when it's destroyed
        // This is the responsibility of "other" to call it
        public void Remove(Collidable other)
        {
            this.Collidables.Remove(other);
        }

        public override void Hide()
        {
            // Do NOT remove these from the collision manager!  They're not there!
            for (SLLNode<Collidable> COIterator = Collidables.Head;
                COIterator != null; COIterator = COIterator.Next)
            {
                GOManager.Instance.Remove(COIterator.GetData());
            }
            base.Hide();
        }

        public override void Show()
        {
            // Do NOT add these to the collision manager!  They're not there!
            for (SLLNode<Collidable> COIterator = Collidables.Head;
                COIterator != null; COIterator = COIterator.Next)
            {
                GOManager.Instance.Add(COIterator.GetData());
            }
            base.Show();
        }

        public override void Dispose()
        {            
            for (SLLNode<Collidable> COIterator = Collidables.Head;
                COIterator != null; COIterator = COIterator.Next)
            {
                Collidables.Remove(COIterator.GetData());
                COIterator.GetData().Dispose();
            }

            base.Dispose();
        }

        public override void Draw()
        {
        }
    }
}