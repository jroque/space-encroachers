﻿using Microsoft.Xna.Framework.Input;

namespace SpaceInvaders
{
    /// <summary>
    /// Class to simplify input handling
    /// </summary>
    static class Input
    {        
        // Helper to not detect multiple key presses
        private static KeyboardState oldKeyState;

        /// <summary>
        /// Stores the current keyboard state into the previous state.
        /// Any polling for keyboard input MUST happen before this 
        /// method is called.  If not, "toggles" will not work.
        /// </summary>
        public static void Update()
        {
            oldKeyState = Keyboard.GetState();
        }
                
        /// <summary>
        /// Reads input from a key.  Toggle forces the function to 
        /// return true only if the key has not been pressed previously.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="toggle"></param>
        /// <returns></returns>
        public static bool IsKeyDown(Keys key, bool toggle)
        {
            bool result = false;
            bool KeyDownOnFrame = Keyboard.GetState().IsKeyDown(key);

            if (toggle)
            {
                if (KeyDownOnFrame && !oldKeyState.IsKeyDown(key))
                {
                    result = true;
                }
                else { /* Do nothing*/}
            }
            else
            {
                result = KeyDownOnFrame;
            }

            return result;

        }

    }
}
