﻿using SpaceInvaders.DataStructures;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{

    class PlayerData
    {
        public SLList<SLList<Alien>> Aliens;
        public SLList<Shield> Shields;
        public Line Line;
        public int NUM_ALIENS;
        public int NUM_LIVES;
        public int CURRENT_LEVEL;
        private bool ACTIVE;
        private AlienDirection ALIEN_DIR = AlienDirection.Horizontal;
        private Vector2 ALIEN_HORIZONTAL;
        public PlayerData OtherPlayer;
        public bool GAME_OVER;
        public int NUM_SHOTS;

        public PlayerData()
        {
            Aliens = new SLList<SLList<Alien>>();
            Shields = new SLList<Shield>();
            NUM_ALIENS = 0;
            NUM_LIVES = 3;
            NUM_SHOTS = 0;
            CURRENT_LEVEL = 1;
            ACTIVE = true;
            GAME_OVER = false;
        }

        #region Initialize
        // Loads all assets
        public void Initialize()
        {
            int LevelOffsetMultiplier = CURRENT_LEVEL - 1;

            // Cap it here otherwise they'll be too fast
            if (LevelOffsetMultiplier > 3)
            {
                LevelOffsetMultiplier = 3;
                
            }

            this.InitializeAliens(
                new Vector2(GameData.ALIEN_START_POS.X, 
                    GameData.ALIEN_START_POS.Y + LevelOffsetMultiplier * GameData.LEVEL_OFFSET));

            this.InitializeShields();
            this.Line = new Line(new Vector2(0, 239 * Constants.SCREEN_SCALE), 255, new AnimationImage(0, @""));
            this.ALIEN_HORIZONTAL = GameData.ALIEN_DIR_HORIZONTAL;
        }
        private void InitializeShields()
        {
            Shields.Add(SpaceFactory.CreateShield(new Vector2(32 * Constants.SCREEN_SCALE, 192 * Constants.SCREEN_SCALE)));
            Shields.Add(SpaceFactory.CreateShield(new Vector2(77 * Constants.SCREEN_SCALE, 192 * Constants.SCREEN_SCALE)));
            Shields.Add(SpaceFactory.CreateShield(new Vector2(122 * Constants.SCREEN_SCALE, 192 * Constants.SCREEN_SCALE)));
            Shields.Add(SpaceFactory.CreateShield(new Vector2(167 * Constants.SCREEN_SCALE, 192 * Constants.SCREEN_SCALE)));
        }
        private void InitializeAliens(Vector2 StartingPosition)
        {

            // Create 11 lists, one for each column
            for (int i = 0; i < GameData.numColumns; i++)
            {
                this.Aliens.Add(new SLList<Alien>());
            }

            for (int i = 0; i < GameData.numRows; i++)
            {
                AlienType alienType;

                switch (i)
                {
                    case 0: alienType = AlienType.B; break;
                    case 1: alienType = AlienType.A; break;
                    case 2: alienType = AlienType.A; break;
                    case 3: alienType = AlienType.C; break;
                    case 4: alienType = AlienType.C; break;
                    default: alienType = AlienType.A; break;
                }

                for (int j = 0; j < GameData.numColumns; j++)
                {
                    // Aliens are 16 x 16 apart

                    Alien P1Alien = SpaceFactory.CreateAlien(alienType, new Vector2(
                        StartingPosition.X + j * 16 * Constants.SCREEN_SCALE,
                        StartingPosition.Y + i * 16 * Constants.SCREEN_SCALE + (GameData.CURRENT_LEVEL - 1) * 16
                        ), 255);

                    // Initialize points.  Need to do it here since we didn't store the alientype
                    if (i == 4 || i == 3)
                    {
                        P1Alien.POINTS = 10;
                    }
                    else if (i == 2 || i == 1)
                    {
                        P1Alien.POINTS = 20;
                    }
                    else if (i == 0)
                    {
                        P1Alien.POINTS = 30;
                    }

                    this.NUM_ALIENS++;
                    this.Aliens.Get(j).Add(P1Alien);
                }
            }
        }
        #endregion

        #region Aliens
        /// <summary>
        /// Selects a random alien to fire
        /// </summary>
        public void AlienFire()
        {
            Constants.DebugPrint("SpaceInvaders: ALIENS!  UNLEASH THE PAIN!");
            int index = Constants.RANDOM.Next(GameData.numColumns);

            // If we come across a null reference, skip it
            while (this.Aliens.Get(index).Head == null)
            {
                if (index < GameData.numColumns - 1)
                {
                    index++;
                }
                else
                {
                    index = 0;
                }
            }

            Alien alienToFire = GameData.CURR_PLAYER.Aliens.Get(index).Head.GetData();
            alienToFire.Fire();
        }

        public void AlienMove()
        {
            bool moveVertical = false;

            SLLNode<SLList<Alien>> column_iterator = this.Aliens.Head;

            // For each column
            while (column_iterator != null)
            {
                SLList<Alien> column = column_iterator.GetData();
                SLLNode<Alien> row_iterator = column.Head;

                // For each row in each column
                while (row_iterator != null)
                {
                    Alien currAlien = row_iterator.GetData();

                    // Actions for current alien
                    switch (this.ALIEN_DIR)
                    {
                        case AlienDirection.Horizontal:
                            currAlien.Position += this.ALIEN_HORIZONTAL;
                            break;
                        case AlienDirection.Vertical:
                            currAlien.Position += GameData.ALIEN_DIR_VERTICAL;
                            break;
                    }

                    // If we have a single alien that is at the edge of the screen
                    if (currAlien.Position.X + currAlien.Width >= GameData.ALIEN_RIGHT_BORDER ||
                            currAlien.Position.X <= GameData.ALIEN_LEFT_BORDER)
                    {
                        moveVertical = true;
                    }
                    else { /**/}

                    // Check possible game over conditions
                    if (currAlien.Position.Y + currAlien.Height > GameData.VERTICAL_THRESHOLD)
                    {
                        this.GAME_OVER = true;
                        this.NUM_LIVES = 0;
                        GameScreen.ThePlayer.ship.Death();
                    }
                    else { /**/}

                    // Cycle the alien's animation
                    currAlien.Animation.UpdateFrame();

                    row_iterator = row_iterator.Next;
                }// End current column

                column_iterator = column_iterator.Next;
            }// End current turn for all aliens

            // Set the next turn
            if (moveVertical)
            {
                // But we just did move vertically
                if (this.ALIEN_DIR == AlienDirection.Vertical)
                {
                    this.ALIEN_DIR = AlienDirection.Horizontal;
                }
                else
                {
                    this.ALIEN_DIR = AlienDirection.Vertical;
                    this.ALIEN_HORIZONTAL = -this.ALIEN_HORIZONTAL;
                }
            }
            else
            {
                // Ensure we move horizontally - Prevents a bug
                this.ALIEN_DIR = AlienDirection.Horizontal;
            }
        }
        #endregion

        #region Show / Hide
        public void Hide()
        {
            if (this.ACTIVE)
            {
                this.ACTIVE = false;
                this.Line.Hide();
                this.HideShields();
                this.HideAliens();
            }
        }

        public void Show()
        {
            if (!this.ACTIVE)
            {
                this.ACTIVE = true;
                this.Line.Show();
                this.ShowShields();
                this.ShowAliens();
            }
        }

        /// <summary>
        /// Hides all the shields without disposing of them
        /// </summary>
        private void HideShields()
        {
            // For each column
            for (SLLNode<Shield> s_iterator = Shields.Head;
                s_iterator != null; s_iterator = s_iterator.Next)
            {
                Shield s = s_iterator.GetData();
                if (s != null)
                {
                    s.Hide();
                }
                else { }
            }
        }

        /// <summary>
        /// Shows all the shields without disposing of them
        /// </summary>
        private void ShowShields()
        {
            // For each column
            for (SLLNode<Shield> s_iterator = Shields.Head;
                s_iterator != null; s_iterator = s_iterator.Next)
            {
                Shield s = s_iterator.GetData();
                if (s != null)
                {
                    s_iterator.GetData().Show();
                }
                else { /**/}
            }
        }

        /// <summary>
        /// Removes aliens from the GOManager
        /// </summary>
        private void HideAliens()
        {
            SLLNode<SLList<Alien>> column_iterator = this.Aliens.Head;

            // For each column
            while (column_iterator != null)
            {
                SLList<Alien> column = column_iterator.GetData();
                SLLNode<Alien> row_iterator = column.Head;

                // For each row in each column
                while (row_iterator != null)
                {
                    Alien currAlien = row_iterator.GetData();
                    if (currAlien != null)
                    {
                        currAlien.Hide();
                    }
                    else { /**/}
                    row_iterator = row_iterator.Next;
                }// End current column

                column_iterator = column_iterator.Next;

            }
        }

        /// <summary>
        /// Reinserts the player's aliens from the GOManager
        /// </summary>
        private void ShowAliens()
        {
            SLLNode<SLList<Alien>> column_iterator = this.Aliens.Head;

            // For each column
            while (column_iterator != null)
            {
                SLList<Alien> column = column_iterator.GetData();
                SLLNode<Alien> row_iterator = column.Head;

                // For each row in each column
                while (row_iterator != null)
                {
                    Alien currAlien = row_iterator.GetData();
                    if (currAlien != null)
                    {
                        currAlien.Show();
                    }
                    row_iterator = row_iterator.Next;
                }// End current column

                column_iterator = column_iterator.Next;

            }
        }

        #endregion

        #region Disposal
        // For Alien Dispose
        public void RemoveFromList(Alien a)
        {
            for (SLLNode<SLList<Alien>> myIterator = Aliens.Head;
                myIterator != null; myIterator = myIterator.Next)
            {
                SLList<Alien> myColumn = myIterator.GetData();
                if (myColumn.Contains(a))
                {
                    this.NUM_ALIENS--;
                    myColumn.Remove(a);
                    break;
                }
                else { /**/}
            }

        }

        // Disposes ALL the aliens
        public void DisposeAliens()
        {
            SLLNode<SLList<Alien>> column_iterator = this.Aliens.Head;

            // For each column
            while (column_iterator != null)
            {
                SLList<Alien> column = column_iterator.GetData();
                SLLNode<Alien> row_iterator = column.Head;

                // For each row in each column
                while (row_iterator != null)
                {
                    Alien currAlien = row_iterator.GetData();
                    currAlien.Dispose();
                    column.Remove(currAlien);
                    row_iterator = row_iterator.Next;
                }// End current column

                column_iterator = column_iterator.Next;

            }// End current turn for all aliens
        }

        public void DisposeShields()
        {
            // For each column
            for (SLLNode<Shield> s_iterator = Shields.Head; 
                s_iterator != null; s_iterator = s_iterator.Next)
            {
                Shield shield = s_iterator.GetData();
                shield.Dispose();
                Shields.Remove(shield);
            }
        }

        public void DisposeAll()
        {
            this.DisposeShields();
            this.DisposeAliens();
        }

        #endregion

    }
}
