﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SpaceInvaders
{
    class Player
    {
        public Ship ship;

        public Player()
        {
            ship = SpaceFactory.CreateShip(Constants.STARTING_POSITION);
        }

        /// <summary>
        /// Polls all the keys for input.  Performs the desired actions upon execution.
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {

            if (Input.IsKeyDown(Keys.Left, false))
            {
                // Make sure we're not at the edge of the screen
                if (ship.Position.X - Constants.PLAYER_SPEED > 17 * Constants.SCREEN_SCALE)
                {
                    ship.Position.X -= Constants.PLAYER_SPEED;
                }
            }

            if (Input.IsKeyDown(Keys.Right, false))
            {
                // Edge of screen check
                if (ship.Position.X + ship.Width - Constants.PLAYER_SPEED < Constants.SCREEN_WIDTH - 22 * Constants.SCREEN_SCALE)
                {
                    ship.Position.X += Constants.PLAYER_SPEED;
                }
            }

            if (Input.IsKeyDown(Keys.Up, false))
            {
                if(Constants.DEBUG){
                    // Make sure we're not at the edge of the screen
                    if (ship.Position.Y - Constants.PLAYER_SPEED > 0)
                    {
                        ship.Position.Y -= Constants.PLAYER_SPEED;
                    }
                }
            }

            if (Input.IsKeyDown(Keys.Down, false))
            {
                if (Constants.DEBUG)
                {
                    // Edge of screen check
                    if (ship.Position.Y + ship.Width - Constants.PLAYER_SPEED < Constants.SCREEN_HEIGHT)
                    {
                        ship.Position.Y += Constants.PLAYER_SPEED;
                    }
                }
            }
            
            if (Input.IsKeyDown(Keys.Space, true))
            {
                ship.Fire(gameTime);
            }
            

            if (Input.IsKeyDown(Keys.F1, true))
            {
                Constants.DEBUG = !Constants.DEBUG;
            }

            if (Input.IsKeyDown(Keys.F3, true))
            {
                if (Constants.VOLUME_BGM == 0)
                {
                    Constants.VOLUME_BGM = 1.0f;
                }
                else
                {
                    Constants.VOLUME_BGM = 0.0f;
                }

                SoundWave.audioEngine.GetCategory("Music").SetVolume(Constants.VOLUME_BGM);                
            }

            if (Input.IsKeyDown(Keys.F4, true))
            {
                if (Constants.VOLUME_SFX == 0)
                {
                    Constants.VOLUME_SFX = 1.0f;
                }
                else
                {
                    Constants.VOLUME_SFX = 0.0f;
                }

                SoundWave.audioEngine.GetCategory("Default").SetVolume(Constants.VOLUME_SFX);
            }

            if (Input.IsKeyDown(Keys.F5, true))
            {
                GameData.CURR_PLAYER.NUM_LIVES++;
            }
        }
    }
}
