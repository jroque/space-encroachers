﻿using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class Bomb : Collidable
    {
        public Bomb(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            this.Direction = Constants.BOMB_DIRECTION;

            // Calculated by measuring assets
            this.Scale = .1f * Constants.SCREEN_SCALE;

            GameData.NUM_BOMBS_ON_SCREEN++;
            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(Missile s)
        {
            SpaceFactory.CreateBombImpact(s.Position);
            this.SetReadyToDispose();
        }

        public override void Collide(Ship s)
        {
            SpaceFactory.CreateBombImpact(new Vector2(this.Position.X - 5, this.Position.Y));
            this.SetReadyToDispose();
        }

        public override void Collide(ShieldPixel a)
        {
            // Generate the explosion
            SpaceFactory.CreateBombImpact(new Vector2(this.Position.X - 5, this.Position.Y));
            this.SetReadyToDispose();
        }

        public override void Dispose()
        {
            GameData.NUM_BOMBS_ON_SCREEN--;
            base.Dispose();
        }

    }
}
