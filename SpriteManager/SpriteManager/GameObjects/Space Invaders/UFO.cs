﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace SpaceInvaders
{
    class UFO : Collidable
    {
        Cue soundCue;
        public int POINTS = 50;

        public UFO(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            // Measured by texture pixels / original pixels
            this.Scale = 0.1f * Constants.SCREEN_SCALE;

            // Play super annoying sound
            soundCue = SoundWave.soundBank.GetCue("Flying_Saucer");
            soundCue.Play();

            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(Missile m)
        {
            SoundWave.Play3D(this, "Dying_Saucer");
            this.SetReadyToDispose();
            base.Collide(m);
        }

        public override void Dispose()
        {
            new Text(this.Position, this.POINTS.ToString());
            // Stop super annoying sound
            soundCue.Stop(AudioStopOptions.Immediate);            
            base.Dispose();
        }



    }
}
