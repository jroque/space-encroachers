﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SpaceInvaders.DataStructures;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class MissileImpact : CollisionGroup
    {
        private StopWatch Timer;

        public MissileImpact(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            Timer = new StopWatch(250, this.SetReadyToDispose);
            this.Collidables = new SLList<Collidable>();
            this.Scale = 8 * Constants.SCREEN_SCALE;
            this.CreateImpact();
            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            this.Timer.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// What happens when a collision happens with this box?  Check the collision with EVERY object...
        /// Pass the collision checking to all the individual objects on the list
        /// </summary>
        /// <param name="other"></param>
        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(ShieldPixel sp)
        {
            this.checkChildren(sp);
        }

        #region ASCII_ART
        /*   01234567
         * 0 X...X..X
         * 1 ..X...X.
         * 2 .XXXXXX.
         * 3 XXXXXXXX
         * 4 XXXXXXXX
         * 5 .XXXXXX.
         * 6 ..X..X..
         * 7 X..X...X
         */

        #endregion

        public void CreateImpact()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    bool draw = true;

                    if      (i == 0 && (j == 1 || j == 2 || j == 3 || j == 5 || j == 6)) { draw = false; }
                    else if (i == 1 && (j == 0 || j == 1 || j == 3 || j == 4 || j == 5 || j == 7)) { draw = false; }
                    else if (i == 2 && (j == 0 || j == 7)) { draw = false; }
                    else if (i == 5 && (j == 0 || j == 7)) { draw = false; }
                    else if (i == 6 && (j == 0 || j == 1 || j == 3 || j == 4 || j == 6 || j == 7)) { draw = false; }
                    else if (i == 7 && (j == 1 || j == 2 || j == 4 || j == 5 || j == 6)) { draw = false; }

                    if (draw)
                    {
                        float scale = Constants.SCREEN_SCALE;
                        Vector2 PixelPosition = new Vector2(this.Position.X + (j * scale), this.Position.Y + (i * scale));
                        ImpactPixel pixel = new ImpactPixel(PixelPosition, 255, new AnimationImage(0, @""));
                        pixel.parent = this;
                        pixel.SpriteBatch = this.SpriteBatch;
                        pixel.Scale = scale;
                        this.Collidables.Add(pixel);
                    }
                }
            }
        }

        public override void Draw()
        {
            // This doesn't get drawn
        }
    }
}
