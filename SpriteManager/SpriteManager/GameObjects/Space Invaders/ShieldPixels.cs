﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace SpaceInvaders
{
    class ShieldPixel : Collidable
    {
        public CollisionGroup parent;

        public ShieldPixel(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
        
        public override void Collision(Collidable other)
        {            
            other.Collide(this);
        }

        public override void Collide(ImpactPixel ip)
        {
            this.SetReadyToDispose();
        }

        /// <summary>
        /// On removal, we make sure the parent does not retain a reference
        /// to this pixel
        /// </summary>
        public override void Dispose()
        {
            parent.Remove(this);
            base.Dispose();
        }
    }
}