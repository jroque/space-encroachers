﻿using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class Alien : Collidable
    {
        public int POINTS = 0;
        public bool ReadytoFire = false;
      
        public Alien(Vector2 Position, int Alpha, Animation Animation)  : base(Position, Alpha, Animation)
        {
            this.Direction = new Vector2(0, 0);

            // Calculated by assets
            this.Scale = .1f * Constants.SCREEN_SCALE;

            CollisionManager.Instance.Add(this);
        }

        public void Fire()
        {
            Constants.DebugPrint("Alien: You finna die, suckaaa!!");
            Bomb myMissile = SpaceFactory.CreateBomb(new Vector2(this.Position.X, this.Position.Y + 40));
        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(Missile m)
        {
            if (Constants.DEBUG)
            {
                SoundWave.Play3D(this, "Jawa_Ootanee");
            }
            else
            {
                SoundWave.Play3D(this, "Enemy_Fire");
            }

            this.SetReadyToDispose();
        }

        public override void Collide(Ship s)
        {
            if (Constants.DEBUG)
            {
                SoundWave.Play3D(this, "Jawa_Ootanee");
                this.SetReadyToDispose();
            }            
        }

        public override void Collide(ShieldPixel s)
        {
            s.SetReadyToDispose();
        }

        // This is necessary as it ties in with the game state
        public override void Dispose()
        {
        
            GameData.CURR_PLAYER.RemoveFromList(this);

            base.Dispose();
        }
    }
}