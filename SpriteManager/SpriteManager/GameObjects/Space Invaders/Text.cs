﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace SpaceInvaders
{
    class Text : GameObject
    {

        public int ExplosionDecayTime = Constants.EXPLOSION_DECAY_TIME * 4;
        StopWatch Timer;
        String text;
        SpriteBatch TextBatch;


        public Text(Vector2 Position, String s) : base(Position, 255, new AnimationImage(0, ""))
        {
            this.text = s;
            Timer = new StopWatch(this.ExplosionDecayTime, this.setReadyToDispose);
            TextBatch = new SpriteBatch(JGame.Instance.GraphicsDevice);
        }

        public void setText(String s)
        {
            this.text = s;
        }


        public void setReadyToDispose()
        {
            this.SetReadyToDispose();
        }

        public override void Update(GameTime gameTime)
        {
            Timer.Update(gameTime);
        }

        public override void Draw()
        {
            TextBatch.Begin();
            TextBatch.DrawString(InternalResourceLoader.InternalFont, 
                this.text, this.Position, this.Tint, 0, Vector2.Zero,
                1.0f, SpriteEffects.None, 0);
            TextBatch.End();
        }

    }
}