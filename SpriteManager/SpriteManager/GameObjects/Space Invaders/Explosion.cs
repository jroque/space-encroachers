﻿using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class Explosion : GameObject
    {

        public int ExplosionDecayTime = Constants.EXPLOSION_DECAY_TIME;
        StopWatch Timer;

        public Explosion(Vector2 Position, int Alpha, Animation animation)
            : base(Position, Alpha, animation) {
                Timer = new StopWatch(this.ExplosionDecayTime, this.setReadyToDispose);
                this.Scale = 0.1f * Constants.SCREEN_SCALE;
        }


        public void setReadyToDispose()
        {
            this.SetReadyToDispose();
        }

        public override void Update(GameTime gameTime)
        {
            Timer.Update(gameTime);
        }

    }
}