﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    /*
     * This class generates the explosions shown on missile/bomb impact.
     * Effect only to deteriorate shields, no other practical effect
     */
    class ImpactPixel : Collidable
    {        
        public CollisionGroup parent;

        public ImpactPixel(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {            
            this.Direction = new Vector2(0, 0);
        }

        public override void Update(GameTime gameTime)
        {
            
            base.Update(gameTime);
        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(ShieldPixel s)
        {
            s.SetReadyToDispose();
        }
        
        public override void Dispose()
        {
            parent.Remove(this);
            base.Dispose();
        }
    }
}
