﻿using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class Ship : Collidable
    {
        bool laserReady = true;
        public bool alive = true;
    
        StopWatch Timer;

        public Ship(Vector2 Position, int Alpha, Animation Animation)  : base(Position, Alpha, Animation)
        {

            Timer = new StopWatch(Constants.LASER_COOLDOWN, this.Reload);

            // Measured by texture pixels / original pixels
            this.Scale = .1f * Constants.SCREEN_SCALE;

            CollisionManager.Instance.Add(this);
            
        }

        public void Fire(GameTime gameTime)
        {
            if (alive)
            {
                
                if (GameData.NUM_MISSILES_ON_SCREEN == 0)
                {
                    if (laserReady)
                    {
                        Constants.DebugPrint("Ship: Firing laser, sir.");
                        Vector2 missilePosition = this.Position;
                        missilePosition.X += (this.Width / 2.0f) - 2;

                        missilePosition.Y -= 0;
                        Missile laser = SpaceFactory.CreateMissile(missilePosition);

                        if (Constants.DEBUG)
                        {
                            SoundWave.Play3D(this, "TIE_Laser");
                        }
                        else
                        {
                            SoundWave.Play3D(this, "Ship_Fire");
                        }

                        GameData.CURR_PLAYER.NUM_SHOTS++;

                        laserReady = false;
                        Timer.ResetTimer();
                    }
                }
                else
                {
                    // Do nothing
                    Constants.DebugPrint("Ship: Unable to fire.  It's clogged, sir!");
                }
            }
            else
            {
                Constants.DebugPrint("Ship: I'm dead, sir!");
            }
            
        }

        /// <summary>
        /// Reload -- Called by the Timer.  Reloads the ship's laser cannons.
        /// </summary>
        private void Reload()
        {
            Constants.DebugPrint("Ship: Reloading laser cannon, sir!");
            this.laserReady = true;
        }

        public void Respawn()
        {
            this.Animation = new AnimationImage(0, @"Images\SHIP_160x80");
            this.alive = true;
        }

        public override void Update(GameTime gameTime)
        {            
            Timer.Update(gameTime);
            base.Update(gameTime);
        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(Alien b)
        {
            if (this.alive && !Constants.DEBUG)
            {
                this.Death();                
            }
            base.Collide(b);
        }

        public override void Collide(Bomb b)
        {
            if (this.alive && !Constants.DEBUG)
            {
                this.Death();                
            }
            base.Collide(b);
        }

        public void Death()
        {
            this.alive = false;
            SoundWave.Play3D(this, "Death");
            this.Animation = new AnimationImage(100, @"Images\SHIP_EXPLODE_1_160x80", @"Images\SHIP_EXPLODE_2_160x80");
            GameData.CURR_PLAYER.NUM_LIVES--;
        }

        public override void Draw()
        {
            base.Draw();
        }

    }
}
