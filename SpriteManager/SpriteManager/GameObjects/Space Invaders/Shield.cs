﻿using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class Shield : CollisionGroup
    {        
        public Shield(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            this.Collidables = new SLList<Collidable>();
            
            // Affects the side of the collision box.
            // "25" is an approximation of how many pixels wide the shield is
            // See ASCII art below
            this.Scale = 25 * Constants.SCREEN_SCALE;
            this.CreateSubShields();
            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw()
        {

        }

        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        /*
         * Register the collision against these objects...
         */ 
        public override void Collide(Alien a)
        {
            this.checkChildren(a);
        }

        public override void Collide(Missile m)
        {
            this.checkChildren(m);
        }

        public override void Collide(Bomb b)
        {
            this.checkChildren(b);
        }

        public override void Collide(MissileImpact mi)
        {
            this.checkChildren(mi);
        }

        public override void Collide(BombImpact bi)
        {
            this.checkChildren(bi);
        }

        #region ASCII_Shield
        /*   0123456789012345678901
         * 0 ....XXXXXXXXXXXXXX....
         * 1 ...XXXXXXXXXXXXXXXX...
         * 2 ..XXXXXXXXXXXXXXXXXX..
         * 3 .XXXXXXXXXXXXXXXXXXXX.
         * 4 XXXXXXXXXXXXXXXXXXXXXX
         * 5 XXXXXXXXXXXXXXXXXXXXXX
         * 6 XXXXXXXXXXXXXXXXXXXXXX
         * 7 XXXXXXXXXXXXXXXXXXXXXX
         * 8 XXXXXXXXXXXXXXXXXXXXXX
         * 9 XXXXXXXXXXXXXXXXXXXXXX
         * 0 XXXXXXXXXXXXXXXXXXXXXX
         * 1 XXXXXXXXXXXXXXXXXXXXXX
         * 2 XXXXXXXXXXXXXXXXXXXXXX
         * 3 XXXXXXXXXXXXXXXXXXXXXX
         * 4 XXXXXXX.......XXXXXXXX
         * 5 XXXXXX.........XXXXXXX
         * 6 XXXXX...........XXXXXX
         * 7 XXXXX...........XXXXXX
         */
        #endregion

        /// <summary>
        /// Creates all the "pixels" for the shield object
        /// </summary>
        public void CreateSubShields()
        {
            for (int i = 0; i < 18; i++)
            {
                for (int j = 0; j < 22; j++)
                {
                    bool draw = true;

                    if      (i == 0)    {   if ((j >= 0 && j <= 3) || (j >= 18 && j <= 21)) { draw = false; }   }
                    else if (i == 1)    {   if ((j >= 0 && j <= 2) || (j >= 19 && j <= 21)) { draw = false; }   }
                    else if (i == 2)    {   if ((j >= 0 && j <= 1) || (j >= 20 && j <= 21)) { draw = false; }   }
                    else if (i == 3)    {   if (j == 0 || j == 21)      { draw = false; }  }
                    else if (i == 14)   {   if ((j >= 7 && j <= 13))    { draw = false; }  }
                    else if (i == 15)   {   if ((j >= 6 && j <= 14))    { draw = false; }  }
                    else if (i == 16)   {   if ((j >= 5 && j <= 15))    { draw = false; }  }
                    else if (i == 17)   {   if ((j >= 5 && j <= 15))    { draw = false; }  }

                    if (draw)
                    {
                        float scale = Constants.SCREEN_SCALE;
                        Vector2 PixelPosition = new Vector2(this.Position.X + (j * scale), this.Position.Y + (i * scale));
                        ShieldPixel subShield = new ShieldPixel(PixelPosition, 128, new AnimationImage(0, @""));
                        subShield.parent = this;
                        subShield.SpriteBatch = this.SpriteBatch;
                        subShield.Scale = scale;
                        this.Collidables.Add(subShield);
                    } //</ifDraw>
                }//</forJ>
            }//</forI>
        }
    }
}