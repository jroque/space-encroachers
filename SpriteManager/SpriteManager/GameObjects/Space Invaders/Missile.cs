﻿using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class Missile : Collidable
    {
        public Missile(Vector2 Position, int Alpha, Animation Animation)  : base(Position, Alpha, Animation)
        {
            this.Direction = Constants.LASER_DIRECTION;
            this.Scale = .1f * Constants.SCREEN_SCALE;
            ++GameData.NUM_MISSILES_ON_SCREEN;
            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            if (this.Position.Y <= Constants.MISSILE_BARRIER)
            {
                SpaceFactory.CreateMissileImpact(Position);
                this.SetReadyToDispose();
            }
            else
            {
                // Do nothing
            }

            base.Update(gameTime);
        }

        public override void Collision(Collidable other)
        {            
             other.Collide(this);
        }

        public override void Collide(Alien a)
        {
            SpaceFactory.CreateExplosion(a.Position);
            GameData.ScoreIncrement(a.POINTS);
            this.SetReadyToDispose();
        }

        public override void Collide(Bomb a)
        {
            SpaceFactory.CreateMissileImpact(this.Position);
            this.SetReadyToDispose();
        }

        public override void Collide(UFO u)
        {
            if (GameData.CURR_PLAYER.NUM_SHOTS % 15 == 0)
            {
                u.POINTS = 300;
            }
            else { /**/}

            GameData.ScoreIncrement(u.POINTS);
            SpaceFactory.CreateExplosion(u.Position);
            this.SetReadyToDispose();
        }

        public override void Collide(Shield s)
        {
            // Don't do anything, wait for the missile to hit the shield pixel
        }

        // Register impact when it hits a pixel
        public override void Collide(ShieldPixel a)
        {
            SpaceFactory.CreateMissileImpact(new Vector2(this.Position.X - 4 * Constants.SCREEN_SCALE, this.Position.Y));
            this.SetReadyToDispose();
        }

        public override void Dispose()
        {
            GameData.NUM_MISSILES_ON_SCREEN--;
            base.Dispose();
        }

    }
}
