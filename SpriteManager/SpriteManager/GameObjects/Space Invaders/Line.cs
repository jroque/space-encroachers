﻿using Microsoft.Xna.Framework;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    /// <summary>
    /// This is for that stupid line at the bottom of the screen
    /// </summary>
    class Line : CollisionGroup
    {        
        public Line(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            this.Collidables = new SLList<Collidable>();

            this.Scale = Constants.SCREEN_WIDTH;
            this.CreateSubShields();

            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// What happens when a collision happens with this box?  Check the collision with EVERY object...
        /// Pass the collision checking to all the individual objects on the list
        /// </summary>
        /// <param name="other"></param>
        public override void Collision(Collidable other)
        {
            SLLNode<Collidable> COIterator = Collidables.Head;

            while (COIterator != null)
            {
                COIterator.GetData().checkCollisions(other);
                COIterator = COIterator.Next;
            }
        }
        public void CreateSubShields()
        {
            for (int i = 0; i < Constants.SCREEN_WIDTH / Constants.SCREEN_SCALE; i++)
            {
                float scale = Constants.SCREEN_SCALE;
                Vector2 PixelPosition = new Vector2(this.Position.X + (i * scale), this.Position.Y);
                ShieldPixel subShield = new ShieldPixel(PixelPosition, 128, new AnimationImage(0, @""));
                subShield.parent = this;
                subShield.Scale = scale;
                this.Collidables.Add(subShield);
            }
        }

        public override void Draw()
        {
            //base.Draw();
        }
    }
}