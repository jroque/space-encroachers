﻿using SpaceInvaders.DataStructures;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    class BombImpact : CollisionGroup
    {
        private StopWatch Timer;

        public BombImpact(Vector2 Position, int Alpha, Animation Animation)
            : base(Position, Alpha, Animation)
        {
            Timer = new StopWatch(250, this.SetReadyToDispose);
            this.Collidables = new SLList<Collidable>();

            // Control the size of the parent box
            this.Scale = 8 * Constants.SCREEN_SCALE;
            this.CreateImpact();
            CollisionManager.Instance.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            Timer.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// What happens when a collision happens with this box?  Check the collision with EVERY object...
        /// Pass the collision checking to all the individual objects on the list
        /// </summary>
        /// <param name="other"></param>
        public override void Collision(Collidable other)
        {
            other.Collide(this);
        }

        public override void Collide(ShieldPixel sp)
        {
            this.checkChildren(sp);
        }

        #region ASCII_ART

        /*   012345
         * 0 ..X...
         * 1 X...X.
         * 2 ..XX.X
         * 3 .XXXX. 
         * 4 X.XXX./// this is what registers when shield hit from top
         * 5 .XXXXX
         * 6 X.XXX.
         * 7 .X.X.X
         */

        #endregion

        public void CreateImpact()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    bool draw = true;

                         if (i == 0 && !(j == 2))                       { draw = false; }
                    else if (i == 1 && !(j == 0 || j == 4))             { draw = false; }
                    else if (i == 2 && (j == 0 || j == 1 || j == 4))    { draw = false; }
                    else if (i == 3 && (j == 0 || j == 5))              { draw = false; }
                    else if (i == 4 && (j == 1 || j == 5))              { draw = false; }
                    else if (i == 5 && (j == 0))                        { draw = false; }
                    else if (i == 6 && (j == 1 || j == 5))              { draw = false; }
                    else if (i == 7 && (j == 0 || j == 2 || j == 4))    { draw = false; }

                    if (draw)
                    {
                        float scale = Constants.SCREEN_SCALE;
                        Vector2 PixelPosition = new Vector2(this.Position.X + (j * scale), this.Position.Y + (i * scale));
                        ImpactPixel pixel = new ImpactPixel(PixelPosition, 255, new AnimationImage(0, @""));
                        pixel.parent = this;
                        pixel.SpriteBatch = this.SpriteBatch;
                        pixel.Scale = scale;
                        this.Collidables.Add(pixel);
                    }else { /**/ }
                }
            }
        }

        public override void Draw()
        {
            // We only draw its pixels
        }
    }
}
