﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace SpaceInvaders
{
    class GameObject
    {
        #region Graphics Vars
        public int Alpha;
        public float Scale;
        public float LayerDepth;

        // Dynamic Height and Width depending on Texture
        public float Height
        {
            get { return this.Animation.GetTexture().Height * Scale; }
        }

        public float Width
        {
            get { return this.Animation.GetTexture().Width * Scale; }
        }

        public Color Tint;
        public Color GetTint() { return new Color(this.Tint.R, this.Tint.G, this.Tint.B, this.Alpha); }

        public BlendState BlendState;
        public SpriteEffects Effects;
        public SpriteBatch SpriteBatch;
        #endregion

        public Vector2 Position;
        public Vector2 Direction;
        public Animation Animation;
        
        private bool ReadyToDispose;
        public  bool GetReadyToDispose() { return this.ReadyToDispose; }
        public  void SetReadyToDispose() { this.ReadyToDispose = true; }

        public GameObject()
        {
            this.Alpha      = 255;
            this.Position   = Vector2.Zero;
            this.Tint       = Color.White;
            this.Scale      = 1.0f;
            this.Effects    = SpriteEffects.None;
            this.LayerDepth = 1;
            this.BlendState = BlendState.AlphaBlend;
            this.Direction  = new Vector2(0, 0);

            GOManager.Instance.Add(this);
        }

        // Specialized constructor, Assumes Alpha Blend
        public GameObject(Vector2 Position, int Alpha, Animation Animation)
            : this()
        {
            this.Position = Position;
            this.Alpha = Alpha;
            this.Animation = Animation;
        }

        // Specialized constructor - specify layer depth
        public GameObject(Vector2 Position, int Alpha, float LayerDepth, Animation Animation)
            : this()
        {
            this.Position = Position;
            this.Alpha = Alpha;
            this.LayerDepth = LayerDepth;
            this.Animation = Animation;
        }

        // Update the GameObject
        public virtual void Update(GameTime gameTime)
        {
            this.Position += this.Direction;
            Animation.Update(gameTime);
        }

        /// <summary>
        /// Default implementation for Draw();
        /// NECESSARY since different objects can be drawn different ways
        /// </summary>
        public virtual void Draw()
        {
            this.SpriteBatch.Draw(
                this.Animation.GetTexture(),
                this.Position,   // Upper left corner to draw sprite
                this.Animation.GetFrame(), // The rectanglular subframe of a texture; Can be null
                this.GetTint(),    // The color to tint a sprite.  Color.White for no tinting
                0,                      // Sprite.Rotation (Radians)
                Vector2.Zero,           // Sprite Origin
                this.Scale,       // Scale factor
                this.Effects,     // SpriteEffects (Flip Horizontal / Vertical)
                this.LayerDepth
            );
        }

        /// <summary>
        /// Adds the object back into the Game Object Manager
        /// </summary>
        public virtual void Show()
        {
            GOManager.Instance.Add(this);
        }

        /// <summary>
        /// Removes the object from the Game Object Manager
        /// </summary>
        public virtual void Hide()
        {
            GOManager.Instance.Remove(this);
        }

        public virtual void Dispose()
        {
            this.Animation.Dispose();
            GOManager.Instance.Remove(this);
        }
    }
}