﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace SpaceInvaders
{
    enum AlienType  {   A,  B,  C,  D   }

    static class SpaceFactory
    {
        public static Alien CreateAlien(AlienType Type, Vector2 StartPosition, int Alpha)
        {
            Alien Alien;
            switch (Type)
            {
                case AlienType.A:
                    Alien = new Alien(StartPosition, Alpha, new AnimationImage(0, @"Images\A1_110x80", @"Images\A2_110x80"));
                    break;
                case AlienType.B:
                    Alien = new Alien(StartPosition, Alpha, new AnimationImage(0, @"Images\B1_80x80", @"Images\B2_80x80"));
                    break;
                case AlienType.C:
                    Alien = new Alien(StartPosition, Alpha, new AnimationImage(0, @"Images\C1_120x80", @"Images\C2_120x80"));
                    break;
                case AlienType.D:
                    Alien = new Alien(StartPosition, Alpha, new AnimationImage(0, @"Images\D1_100x80", @"Images\D2_100x80"));
                    break;
                default:
                    Alien = new Alien(StartPosition, Alpha, new AnimationImage(50, @"Images\C1_120x80", @"Images\C2_120x80",
                                            @"Images\B1_80x80", @"Images\B2_80x80", @"Images\A1_110x80", @"Images\A2_110x80"));
                    break;                    
            }
            return Alien;
        }

        public static Alien CreateRandomAlien(Vector2 StartPosition, int Alpha)
        {
            AlienType myAlienType;

            switch (Constants.RANDOM.Next(4))
            {
                case 0:
                    myAlienType = AlienType.A;
                    break;
                case 1:
                    myAlienType = AlienType.B;
                    break;
                case 2:
                    myAlienType = AlienType.C;
                    break;
                case 3:
                    myAlienType = AlienType.D;
                    break;
                default:
                    myAlienType = AlienType.A;
                    break;

            }

            return SpaceFactory.CreateAlien(myAlienType, StartPosition, Alpha);
        }

        // Randomize the bomb
        public static Bomb CreateBomb(Vector2 Position)
        {
            Bomb myBomb = null;

            if (Constants.RANDOM.Next(10) % 2 == 0)
            {
                myBomb = new Bomb(Position, 255, new AnimationImage(25, @"Images\BOMB1_1_30x70",
                                                                        @"Images\BOMB1_2_30x70",
                                                                        @"Images\BOMB1_3_30x70",
                                                                        @"Images\BOMB1_4_30x70"));
            }
            else
            {
                myBomb = new Bomb(Position, 255, new AnimationImage(25, @"Images\BOMB2_1_30x70",
                                                                        @"Images\BOMB2_2_30x70",
                                                                        @"Images\BOMB2_3_30x70",
                                                                        @"Images\BOMB2_4_30x70",
                                                                        @"Images\BOMB2_5_30x70",
                                                                        @"Images\BOMB2_6_30x70",
                                                                        @"Images\BOMB2_7_30x70"));
            }
            return myBomb;
        }

        public static Bomb CreateBombA(Vector2 Position)
        {
            return new Bomb(Position, 255, new AnimationImage(25, @"Images\BOMB1_1_30x70",
                                                                        @"Images\BOMB1_2_30x70",
                                                                        @"Images\BOMB1_3_30x70",
                                                                        @"Images\BOMB1_4_30x70"));
        }
        public static Bomb CreateBombB(Vector2 Position)
        {
            return new Bomb(Position, 255, new AnimationImage(25, @"Images\BOMB2_1_30x70",
                                                                        @"Images\BOMB2_2_30x70",
                                                                        @"Images\BOMB2_3_30x70",
                                                                        @"Images\BOMB2_4_30x70",
                                                                        @"Images\BOMB2_5_30x70",
                                                                        @"Images\BOMB2_6_30x70",
                                                                        @"Images\BOMB2_7_30x70"));
        }

        public static MissileImpact CreateMissileImpact(Vector2 Position)
        {
            return new MissileImpact(Position, 255, new AnimationImage(0, ""));
        }

        public static BombImpact CreateBombImpact(Vector2 Position)
        {
            return new BombImpact(Position, 255, new AnimationImage(0, ""));
        }

        public static UFO CreateUFO(Vector2 Position)
        {
            return new UFO(Position, 255, new AnimationImage(0, @"Images\UFO_160x70"));
        }

        public static Ship CreateShip(Vector2 Position)
        {
            return new Ship(Position, 255, new AnimationImage(0, @"Images\SHIP_160x80"));
        }

        public static Missile CreateMissile(Vector2 Position)
        {
            return new Missile(Position, 255, new AnimationImage(0, @"Images\MISSILE_10x40"));
        }

        public static Explosion CreateExplosion(Vector2 Position)
        {
            return new Explosion(Position, 255, new AnimationImage(0, @"Images\EXPLOSION_130x80"));
        }

        public static Shield CreateShield(Vector2 Position)
        {
            return new Shield(Position, 255, new AnimationImage(0, ""));
        }
    }
}
