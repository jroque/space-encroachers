﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SpaceInvaders.DataStructures;

namespace SpaceInvaders
{
    class HUD
    {
        private SpriteBatch spriteBatch;
        private SLList<HUDText> HUD_List;
        public  static HUD Instance = new HUD();
        public  bool drawLives = false;
        public bool drawP2Score = true;

        class HUDText
        {
            public String Text;
            public Vector2 Position;
            public Color Color;

            public HUDText(String text, Vector2 position, Color color)
            {
                this.Text = text;
                this.Position = position;
                this.Color = color;
            }
        }

        HUDText P1_SCORE        = new HUDText("SCORE<1>", new Vector2(9 * Constants.SCREEN_SCALE, 9 * Constants.SCREEN_SCALE), Color.White);
        HUDText P2_SCORE        = new HUDText("SCORE<2>", new Vector2(153 * Constants.SCREEN_SCALE, 9 * Constants.SCREEN_SCALE), Color.White);
        HUDText HI_SCORE        = new HUDText("HI-SCORE", new Vector2(81 * Constants.SCREEN_SCALE, 9*Constants.SCREEN_SCALE), Color.White);
        HUDText P1_SCORE_NUM    = new HUDText(GameData.P1_SCORE.ToString(), new Vector2(25 * Constants.SCREEN_SCALE, 25 * Constants.SCREEN_SCALE), Color.White);
        HUDText P2_SCORE_NUM    = new HUDText(GameData.P2_SCORE.ToString(), new Vector2(169 * Constants.SCREEN_SCALE, 25 * Constants.SCREEN_SCALE), Color.White);
        HUDText HI_SCORE_NUM    = new HUDText(GameData.HI_SCORE.ToString(), new Vector2(89 * Constants.SCREEN_SCALE, 25 * Constants.SCREEN_SCALE), Color.White);
        HUDText LIVES        = new HUDText(0.ToString(), new Vector2(9 * Constants.SCREEN_SCALE, 241 * Constants.SCREEN_SCALE), Color.White);
        HUDText CREDIT          = new HUDText("CREDIT " + GameData.NUM_CREDITS.ToString(), 
                                    new Vector2(137 * Constants.SCREEN_SCALE, 241 * Constants.SCREEN_SCALE), Color.White);
        HUDText RUNTIME         = new HUDText("", new Vector2(50, 600), Color.White);

        HUDText GAME_OVER       = new HUDText("GAME OVER", new Vector2(73 * Constants.SCREEN_SCALE, 57 * Constants.SCREEN_SCALE), Constants.TOP_COLOR);

        Texture2D SHIP = TextureManager.getInstance().Add(@"Images\SHIP_160x80");

        private HUD()
        {
            this.spriteBatch = new SpriteBatch(JGame.Instance.GraphicsDevice);
            this.HUD_List = new SLList<HUDText>();
        }

        public void Initialize()
        {
            this.HUD_List.Add(P1_SCORE);
            this.HUD_List.Add(P2_SCORE);
            this.HUD_List.Add(HI_SCORE);
            this.HUD_List.Add(P1_SCORE_NUM);
            //this.HUD_List.Add(P2_SCORE_NUM);
            this.HUD_List.Add(HI_SCORE_NUM);
            this.HUD_List.Add(CREDIT);
        }

        /// <summary>
        /// Update the HUD -- Fetch variable data
        /// </summary>
        /// <param name="gameTime"></param>
        public void Update(GameTime gameTime)
        {
            P1_SCORE_NUM.Text = GameData.P1_SCORE.ToString();
            P2_SCORE_NUM.Text = GameData.P2_SCORE.ToString();
            LIVES.Text = GameData.CURR_PLAYER.NUM_LIVES.ToString();
            HI_SCORE_NUM.Text = GameData.HI_SCORE.ToString();
            RUNTIME.Text = gameTime.TotalGameTime.ToString();
            CREDIT.Text = "CREDIT " + GameData.NUM_CREDITS.ToString();
        }

        /// <summary>
        /// Draw the HUD
        /// </summary>
        /// <param name="gameTime"></param>
        public void Draw(GameTime gameTime)
        {
            float textScale = 1.0f;
            spriteBatch.Begin();
            SLLNode<HUDText> HT_Iterator = this.HUD_List.Head;

            while (HT_Iterator != null)
            {
                HUDText hudText= HT_Iterator.GetData();
                spriteBatch.DrawString(InternalResourceLoader.InternalFont, hudText.Text, hudText.Position, hudText.Color, 0, Vector2.Zero,
                    textScale, SpriteEffects.None, 0);
                HT_Iterator = HT_Iterator.Next;
            }

            if (this.drawP2Score)
            {
                HUDText hudText = this.P2_SCORE_NUM;
                spriteBatch.DrawString(InternalResourceLoader.InternalFont, hudText.Text, hudText.Position, hudText.Color, 0, Vector2.Zero,
                    textScale, SpriteEffects.None, 0);
            }


            // Draw ship lives HUD
            if (this.drawLives)
            {
                spriteBatch.DrawString(InternalResourceLoader.InternalFont, this.LIVES.Text, this.LIVES.Position, 
                    this.LIVES.Color, 0, Vector2.Zero, textScale, SpriteEffects.None, 0);

                for (int i = 0; i < GameData.CURR_PLAYER.NUM_LIVES - 1; i++)
                {
                    spriteBatch.Draw(
                    SHIP,
                    new Vector2(55 + i * 40, 605),   // Upper left corner to draw sprite
                    null, // The rectanglular subframe of a texture; Can be null
                    Color.Lime,    // The color to tint a sprite.  Color.White for no tinting
                    0,                      // Sprite.Rotation (Radians)
                    Vector2.Zero,           // Sprite Origin
                    0.1f * Constants.SCREEN_SCALE,       // Scale factor
                    SpriteEffects.None,     // SpriteEffects (Flip Horizontal / Vertical)
                    0
                    );
                }
            }
            else { /**/}

            if (GameData.GAME_OVER)
            {
                spriteBatch.DrawString(InternalResourceLoader.InternalFont, GAME_OVER.Text, GAME_OVER.Position, GAME_OVER.Color, 0, Vector2.Zero,
                    textScale, SpriteEffects.None, 0);
            }
            else { /**/}

            spriteBatch.End();
        }
    }
}