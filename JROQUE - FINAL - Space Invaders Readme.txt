Space Encroachers
A SE456 Project
Author: Jeremiah Roque

======================================
FEATURES
======================================
1. Able to control a ship that can move and fire
2. Collisions registered between bombs/shields/ship, missile/shield/alien/ufo, alien/ship
3. Shield collision system shows deterioration on shields when impacted by a bomb or missile
4. Aliens properly advance towards the bottom of the screen
5. Decaying green line at the bottom is present
6. HUD is functional for Player 1.
7. Resource management properly resets the game screen if desired at the end of a match
8. DEBUG MODE - F1
9. Support for Stereo Sound
10. Animation system supports both spritesheets as well as image-per-frame animations.
11. Collision system uses rectangles to detect collision between different objects.
12. Collision system supports grouping so smaller collisions can be checked only when necessary.
13. Support for the bottom alien in each column firing on a random basis.
14. Input Manager properly supports "toggle" buttons where a key has to be released to register again, as well as holds.
15. SpaceFactory makes it easy for programmers to create objects wherever needed
16. Size and aspect ratio should be roughly 2.5x the original game
17. Game properly detects "GAME OVER" and "ON TO NEXT LEVEL" scenarios
18. HUD shows lives counter ONLY in game.
19. HUD appears on both in-game and on the Main Menu.
20. Alien speed is based strictly on how many aliens are on screen.
21. UFOs appear at random intervals.
22. Points are properly allocated.
23. All sound effects are properly implemented, even with a bit of reverb.
24. Proper shield decay system will emulate original game
25. Proper emulation of different missile "explosions"
26. Proper Space Invaders Text
27. Near pixel-perfect placement of items
28. Successive difficulty levels are harder -- Aliens start off faster
29. Successive difficulty levels are harder -- Aliens start closer to bottom
30. Credits are actually used up when game starts
31. UFOs now appear from level to right
32. Player 2's score is hidden when in Single Player mode
33. UFOs no longer appear if the number of aliens on screen is 8 or less (source: http://tips.retrogames.com/gamepage/invaders.html)
34. Extra life at 1500 points, with 1up sound
35. Pause feature added
36. Player 2 Support
37. UFO"s point value now appears when hit by a missile.
38. Incorporated behaviour where if every 15th of a player's missile hits a UFO, the UFO becomes worth 300 points.
39. Put a cap on how far down the aliens start when getting to higher levels -- Prevents them starting below shields.
40. High Score is updated in real time so that the current player feels like a badass when reachiing high score.

======================================
OMITTED FEATURES
======================================
1. UFO launches one bomb at a time. -- Unable to replicate situation in original game.  Behavior will not be incorporated.
2. When outside columns of aliens are destroyed, alien movement has greater range in the horizontal direction.  -- Unable to replicate situation in original game.  Behavior will not be incorporated.